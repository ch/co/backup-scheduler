# Please don't edit this file directly!
# Instead, modify the packaged version as provided by our backup-scheduler repo
# We flag this as a config file, so if it's edited here we'll get prompted about
# differences when upgrading the package version

# Files to be ignored in rsnapshots
/dev/
/proc/
# Removed, Redhat machines keep useful stuff in here like my custom specfiles. CEN
#/usr/src/
/sys/
/var/lib/hobbit/tmp/
/var/lib/xymon/tmp/
/tmp/
/var/tmp/
/var/cache/apt/
/var/cache/fontconfig/
/var/cache/dictionaries-common/
/var/cache/ldconfig/
/var/cache/man/
/var/cache/apache2/
/var/log/ntpstats/
/var/lib/apt/lists
/var/lib/mediawiki/images/temp/
/var/lib/mediawiki/images/tmp/
/var/run/
/run/
/var/cache/locate/locatedb
/var/lib/mlocate/
*.pyc
/var/cache/samba/
/var/lib/php5/sess_*
/var/cache/nscd/
/var/lib/ntp/ntp.drift
## mysql binary logfiles change too frequently and aren't useful for a restore
# (That's what the mysqldump output is for!)
/var/log/mysql/mysql-bin.*
/var/lib/mysql/*/*.MY*

/etc/swapfile*
/swap.img

.mozilla/firefox/*/Cache/
/home/*/.cache/

# And we dont want to back up emails
.thunderbird/*.default/Cache/
.thunderbird/*.default/ImapMail/

# locale builds files here, no need to back up
/usr/share/locale/
# Likewise with zoneinfo
/usr/share/zoneinfo/

# We don't need to backup these files which come across in deb files
/var/lib/dpkg/info

# Automounter
/net

# Now we mount /rsnapshots on clients, let's not get into a loop. Again.
/netscratch
/rsnapshots
# This is where log info goes
/rsnapshot
# Ugh - nspluginwrapper generates Gigs of rubbish
.xsession-errors
.xsession-errors.old
# this is by definition for temporary stuff. 
/mnt
# NFS mount point on workstations
/shared
# This is where we remount NFS-exported stuff!
/srv/nfs4

# This gets quite big under Ubuntu and is mainly populated by useless symlinks to files from packages.
/usr/share/help

# also gets rather big on Ubuntu - really shouldn't be anything non-deb provided so 
# lets not waste time having rsync enumerate their contents
/usr/share/doc
/usr/share/texlive

.zfs/
/var/cache/apt-xapian-index

# cluster scratch stuff
/sharedscratch
/nodescratch
# Cluster NFS mount
/usr/local/clustersoftware

# This causes read errors on Ubuntu workstations
/var/lib/lightdm/.gvfs
/home/*/.gvfs

# No external hard drives
/media

# don't back up /scratch etc
/scratch
/scratch2

# update-grub might mount filesystems here
/var/lib/os-prober/mount

# Ansible's temp data isn't helpful
/root/.ansible
