#!/bin/bash
# To move a backup task onto a new zpool
# This is for use on machines like nest-backup, cerebro-backup where there are multiple
# backup tasks for each host so move-machine-to-zpool.sh doesn't work. 
# It assumes the required parent ZFSes exist on the new zpool . 

# Load some settings
. /etc/default/zfs-backup

function dosql() {
 SQL="$1"
 psql -q -t -c "$SQL"
}

# 
if [ -z "$1" ] ; then
 echo Usage: $0 zfsname [new-zpool]
 echo Moves the backup in zfsname onto a new zpool
 echo If the new-zpool parameter is empty, the one with the most space will automatically be chosen
 exit 1
fi

SOURCE=$1
OZ=${SOURCE%%/*}

# Which target is it?
if [ -z "$TGT" ] ; then
 echo Finding a target ZFS automatically
 ZPOOL=`zfs list -H -oname -S avail -d0 | grep -v zroot | grep -v $OZ | head -n1`
 NZ=$ZPOOL
fi

TGT=${SOURCE/$OZ/$NZ}

echo moving $SOURCE to $TGT
set -e
RUNNING=`dosql "select count(*) from backup_log natural join backup_task where zfs_target='$SOURCE' and ended_processing is null and started_processing is not null"`
RUNNING=`echo $RUNNING` # chomp
#echo RUNNING $RUNNING
if ! [ "$RUNNING" = 0 ] ;  then
 echo $SOURCE has a job running
 exit 3
fi

# Disable this host
dosql "update host set disabled='t' from backup_task where zfs_target='$SOURCE' and backup_task.host_id = host.host_id;"
# De-queue jobs for this host
dosql "delete from backup_log where backup_task_id in (select backup_task_id from backup_task where zfs_target='$SOURCE') and ended_processing is null and started_processing is null;"

# Update tasks
dosql "update backup_task set zfs_target=regexp_replace(zfs_target,'^$SOURCE','$TGT') where zfs_target ='$SOURCE'";


# Move ZFS
TIME=`date +%s`
zfs snapshot -r $SOURCE@$TIME
zfs hold zfs-moving $SOURCE@$TIME
SIZE=`zfs list -H -o used $SOURCE`
echo Sending  ZFS [ $SIZE]
zfs send -R $SOURCE@$TIME | mbuffer | zfs receive -F $TGT

# Re-enable host
echo Re-enabling backup task
dosql "update host set disabled='f' from backup_task where zfs_target='$TGT' and backup_task.host_id = host.host_id;"
# Tidy up
#echo Removing old ZFS, snapshots etc
#for SN in `zfs list -r -t snapshot -H -o name $SOURCE/$FQDN ` ; do for H in `zfs holds -H $SN | awk ' { print $2 ; } '` ; do zfs release $H $SN ; done ; done
#zfs destroy -r $SOURCE/$FQDN
