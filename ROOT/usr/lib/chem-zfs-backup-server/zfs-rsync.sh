#!/bin/bash
# To backup a machine using ZFS on the server and rsync to the client
if [ -t 1 ] ; then 
set -xv
fi
#/usr/local/backupsv2/bin/zfs-rsync.sh www-fleming.ch.private.cam.ac.uk / zpool1/www-fleming.ch.private.cam.ac.uk/zfs-rsnap
HN=$1
SRC=$2
DST=$3

TAG=${SRC//\//.}

LOGDIR=/var/log/chem-zfs-backup-server/
CONFDIR=/etc/chem-zfs-backup-server/zfs-rsync.d

TIME=`date +%s`
LOGFILE=$LOGDIR${HN}_${TAG}.log

if [ -z "$DST" ] ; then
 echo Use: $0 hostname destination
 exit 1
fi

echo $(date): running $0 $1 $2 $3 >>$LOGFILE

if [ -f $CONFDIR/global_config ] ; then
 # This file can define global options for rsync
 # and the default rsync command
 echo Including settings from $CONFDIR/global_config
 . $CONFDIR/global_config
fi

if [ -f $CONFDIR/${HN}_${SRC//\//.} ] ; then
 # This file can define PRE and POST as scripts which are run
 echo Including settings from $CONFDIR/${HN}_${TAG}
 . $CONFDIR/${HN}_${TAG}
 export SSHUSER
fi

if [ -f $CONFDIR/$HN/sshconfig ] ; then
  export SSHCONFIGFILE=$CONFDIR/$HN/sshconfig
fi

# Prepare the host if required
if [ ! -z "$PRE" ] ; then
 $PRE $HN $SRC $DST >>$LOGDIR/${HN}_${TAG}-pre.log 2>&1

 PRE_EXIT=$?
 if [ $PRE_EXIT -ne 0 ] ; then
   echo PRE script $PRE $HN $SRC $DST exited with $PRE_EXIT >>$LOGFILE
   exit $PRE_EXIT
 fi

fi

# Do the backup
echo Making snapshot with zfs snapshot $DST@$TIME 2>&1 >>$LOGFILE
zfs snapshot $DST@$TIME
#DEST=`mount | awk ' $1=="'$DST'" { print $3 } '`
# Maybe we should mount the ZFS
MNTED=`zfs get -H -ovalue mounted $DST`
if [ $MNTED = no ] ; then
 echo Mounting ZFS $DST 2>&1 >>$LOGFILE
 zfs mount $DST
fi
DEST=`zfs get -H -ovalue mountpoint "$DST"`
[ -d $DEST/.zfs ]  || exit 2 # Quit if no .zfs directory

# Check for an sshfs exclude file
if [ -f $CONFDIR/$HN/sshfs ]
then
 SSHFS=$CONFDIR/$HN/sshfs
fi

if [ -n "$SSHCONFIGFILE" ] ; then
  SSH="ssh -F $SSHCONFIGFILE "
else
  SSH="ssh "
fi

echo Running rsync >> $LOGFILE
echo ${RSYNC_CMD:-rsync} -e \"$SSH \" -a ${SSHUSER:-root}@$HN:$SRC $DEST/ --exclude-from=$CONFDIR/$HN/exclude --exclude-from=$CONFDIR/global_ignore  ${SSHFS:+--exclude-from=$SSHFS} --delete --delete-excluded $GLOBAL_RSYNCARGS $RSYNCARGS 2>&1 >>$LOGFILE
set +e
${RSYNC_CMD:-rsync} -e "$SSH " -a ${SSHUSER:-root}@$HN:$SRC $DEST/ --exclude-from=$CONFDIR/$HN/exclude --exclude-from=$CONFDIR/global_ignore  ${SSHFS:+--exclude-from=$SSHFS}  --delete --delete-excluded $GLOBAL_RSYNCARGS $RSYNCARGS >>$LOGFILE 2>&1
EXIT=$?
echo Finished rsync at `date` with exit $EXIT >> $LOGFILE
# We do not care about rsync exit code 24 which is 'partial transfer due to vanished source files'
if [ $EXIT -eq 24 ]
then
 EXIT=0
fi
# We should always tidy up.
if [ ! -z "$POST" ] ; then
 $POST $HN $SRC $DST >>$LOGDIR/${HN}_${TAG}-post.log 2>&1
fi
exit $EXIT
