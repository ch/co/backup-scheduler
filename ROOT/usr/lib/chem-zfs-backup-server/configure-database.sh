#!/bin/bash
# A quick and dirty script to configure the database for our backups

DBHOST=$1
DBBASE=$2
DBUSER=$3
DBPASS=$4
DBPORT=$5

CONFIG=/etc/default/zfs-backup
CONFDIR=/etc/chem-zfs-backup-server

# Correct arguments?
if [ -z "$DBPORT" ] ; then
 echo Use: $0 DBHOST DBBASE DBUSER DBPASS DBPORT
 echo 'DBHOST - hostname of postgresql server (/var/run/postgresql/ for localhost via unix domain socket)'
 echo 'DBBASE - name of database (will be created)'
 echo 'DBUSER - username (will be created)'
 echo 'DBPASS - password'
 echo 'DBPORT - network port (5432)'
 exit 1
fi


# Running as root?
if [ "$UID" != 0 ] ; then
 echo $0 must run as root
 exit 2
fi

# Already configured?
if (! (grep -q %USERNAME% $CONFIG \
   && grep -q %DATABASE%  $CONFIG \
   && grep -q %PORT%      $CONFIG \
   && grep -q %HOSTNAME%  $CONFIG)) ; then
 echo $CONFIG is already configured
 exit 3
fi

if [ -f $CONFDIR/pgpass ] ; then
 echo $CONFDIR/pgpass exists
 exit 4
fi

if [ -f $CONFDIR/pgpass-hobbit ] ; then
 echo $CONFDIR/pgpass-hobbit exists
 exit 5
fi

PSQLARGS="${DBHOST:+-h ${DBHOST}}"

echo -n Creating database ...' '
set -e
OUTFILE=`tempfile`
su -c "createdb \"$DBBASE\"" postgres
su -c "createuser -D -R -S \"$DBUSER\"" postgres 
su -c "psql $PSQLARGS \"$DBBASE\" -c \"alter user $DBUSER encrypted password '$DBPASS'\""  postgres >>$OUTFILE 2>&1
DIR=`dirname $0`
cat $DIR/DB/database.sql | su -c "psql $PSQLARGS \"$DBBASE\" " postgres >>$OUTFILE 2>&1
su -c "psql $PSQLARGS \"$DBBASE\" -c \"grant backup_operator to $DBUSER\" " postgres >>$OUTFILE 2>&1
if [ "${DBHOST%/}" == "/var/run/postgresql" ]
then
 PGHOST=localhost
else
 PGHOST=$DBHOST
fi
echo ${PGHOST}:${DBPORT}:${DBBASE}:${DBUSER}:${DBPASS} >$CONFDIR/pgpass
chmod 0600 $CONFDIR/pgpass
cp $CONFDIR/pgpass $CONFDIR/pgpass-hobbit
if getent passwd hobbit >/dev/null
then
  chown hobbit $CONFDIR/pgpass-hobbit
else
  chown xymon $CONFDIR/pgpass-hobbit
fi
echo " [details in $OUTFILE] OK"

echo -n Writing config file ...' '
sed -i "s|%USERNAME%|$DBUSER|" $CONFIG
sed -i "s|%DATABASE%|$DBBASE|" $CONFIG
sed -i "s|%PORT%|$DBPORT|"     $CONFIG
sed -i "s|%HOSTNAME%|$DBHOST|" $CONFIG
echo OK


echo Add this line to your pg_hba.conf file /above/ any other 'local' lines
echo ---------------------------------------------------------------------
echo local ${DBBASE} ${DBUSER} md5 
echo ---------------------------------------------------------------------


