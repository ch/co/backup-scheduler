#!/usr/bin/perl
# -I /usr/local/lib/perl/site_perl
# A perl-based backup script: Read docco 

$|=1;

=head1 NAME
 threaded-backup.pl
=cut

=head1 DECRIPTION
 A perl-based backup script. Configuration and logging data are held in an sqlite3 database.
 A query is run against this database to determine which machines to back up. Machines to
 back up are sent through queues to writer-threads, one for each VG, where the heavy lifting
 happens.
# zpool 6 on plea
=cut

# threaded-backup.h
sub getvgs();    # Get a list of VGs
sub topsighup(); # Handle a TERM at the top level
sub topsigint(); # Handle an INT at the top level
sub tidydatabase(); # Remove defunct entries from the database due to crashes/TERMs

use strict;
use threads;
use LockFile::Simple;
use Thread::Queue;
use DBI;
use Net::Ping;
use JSON;
use POSIX qw(setsid);
use IPC::Open3;
use Getopt::Long;

use ZFS::Host;
use ZFS::Config;

# Check we're not running in parallel
my $lockmgr=LockFile::Simple->make(-autoclean=>1, -max=>1, -hold=>0, -ext=>'.pid');
# Check to see whether our lockfile exists
if (-e "/var/run/threaded-backup.pid") {
 my $fh;
 open ($fh,"/var/run/threaded-backup.pid") || die "Cannot open /var/run/threaded-backup.pid\n";
 my $pid=<$fh>;
 close ($fh);
 chomp($pid);
 if (-e "/proc/$pid" ) {
  print "PID file /var/run/threaded-backup.pid exists and so does the process identified therein\n";
  exit(2);
 }
 unlink("/var/run/threaded-backup.pid");
}

my $CONFFILE='/etc/default/zfs-backup';
$ENV{PATH} = "$ENV{PATH}:/usr/lib/chem-zfs-backup-server/";
my $background='';
GetOptions("background"=>\$background) or die "Cannot parse command line\n";

# Read the config file
my $fh;
my $l;
open ($fh,$CONFFILE) || die ("Error : $!\n");
while ($l=<$fh>) {
 chomp($l);
 $l =~ s/#.*//;
 next if ($l eq '');
 while ($l =~ /\\$/) {
  $l =~ s/\\$//;
  $l.=<$fh>;
  chomp($l);
 }
 if ($l =~ /(.*)=(.*)/) {
   my $key = $1;
   my $val = $2;
   $val =~ s/\'//g;
   $ENV{$key}=$val;
 }
}
my $zpool_writers={split /[;:]/, $ENV{'ZTHREADS'}};

# Constants: M_ - messages; D_ - destinations; SLEEP
use constant {
 D_MAIN=>0x01,
 D_CRON=>0x02,
 D_DISPATCHER=>0x04,
 D_WRITER=>0x08,
 D_MASK=>0x0f,

 M_STOP=>0x10,
 M_MASK=>0xf0,

 SLEEP_WATCHER=>1,
 SLEEP_WRITER=>1,
 SLEEP_CRON=>2,
};

# Don't fork if we're running interactively.
# daemonize http://www.andrewault.net/2010/05/27/creating-a-perl-daemon-in-ubuntu/
if ($background) {
 chdir '/';
 umask 0;
 open STDIN, '/dev/null' or die "Cannot read /dev/null: $!";
 open STDERR, '>>/dev/null' or die "Cannot write to /dev/null: $!";
 open STDOUT, '>>/dev/null' or die "Cannot write to /dev/null: $!";
 defined (my $pid=fork) or die "Cannot fork: $!";
 exit if $pid;
 POSIX::setsid() or die "Cannot start a new session: $!";
};
$lockmgr->lock("/var/run/threaded-backup") || die "Cannot lock /var/run/threaded-backup.pid\n";

# Remove any interrupted jobs. 
&tidydatabase;

# A queue of messages broadcast to all threads.
my $controlq: shared=Thread::Queue->new();

# Create some writers
my $writers={};
my $writerqs={};

# Handle singals: 
#  TERM does a graceful shutdown; 
#  INT doesn't yet do an immediate shutdown
$SIG{TERM}=\&topsighup;
$SIG{INT}=\&topsigint;
$SIG{USR1}=\&topsigusr1;

# Need to know how many threads we should have ; if one dies we all stop
my $threadcount=0;
# We shall create N writer threads for each zpool
my @zpools=&getzpools();
foreach my $zpool (@zpools) {
 my $tq:shared=Thread::Queue->new();
 $writerqs->{$zpool}=$tq;
 # Create writer threads
 for (my $i=0; $i<(exists($zpool_writers->{$zpool})?$zpool_writers->{$zpool}:1); $i++) {
  $writers->{"$zpool.$i"}=threads->create('writer',$zpool,$tq,"$zpool.$i");
  $threadcount++;
 }
}
# And one thread to control the writers
my $cron=threads->create('cron',$writerqs);
$threadcount++;

# As long as we have all those threads, just keep on running
while (scalar(threads->list(threads::running))==$threadcount) {
  sleep(2);
}

# Close-down code
print "Shutting down: only ".scalar(threads->list(threads::running))." threads running\n";
# Send cron the stop signal if it's still running
$controlq->enqueue(D_CRON|M_STOP) if $cron->is_running();
# Send writers the stop signal
$controlq->enqueue(D_WRITER|M_STOP);

# Wait for all threadsA
$SIG{TERM}='DEFAULT';
$SIG{INT}='DEFAULT';
$cron->join();
foreach my $wrthread (keys(%{$writers})) { $writers->{$wrthread}->join();}
exit;

# Handle SIGTERM: send stop messages to all threads and stop cleanly.
sub topsighup() {
 print "Gracefully terminating\n";
 $controlq->enqueue(D_CRON|M_STOP) if (defined($cron) && $cron->is_running());
 $controlq->enqueue(D_WRITER|M_STOP);
 print "Stop messages sent\n";
}

# Handle SIGINT: send INT signals to all threads and stop immediately.
sub topsigint() {
 print "INT caught: stopping\n";
 $cron->kill('SIGINT') if defined($cron);
 foreach my $zpool (keys(%{$writers})) { $writers->{$zpool}->kill('SIGINT');};
}

# catch a USR1 signal and spread it around
sub topsigusr1() {
 # Catch USR1 - reopen log file
 $cron->kill('SIGUSR1') if defined($cron);
 foreach my $zpool (keys(%{$writers})) { $writers->{$zpool}->kill('SIGUSR1');};
}

# Thread which determines which machines to backup.
sub cron {
 my $qs=shift;
 print "Cron thread started\n";
 # Begin a log file
 my $logfile="/var/log/chem-zfs-backup-server/cron-thread.log";
 my $logfh;
 open $logfh,">>$logfile";
 # Unbuffer the log file
 select((select($logfh),$|=1)[0]);
 # Handle log-file closure
 $SIG{'USR1'}=sub { 
      close($logfh);
      open $logfh,">>$logfile";
      select((select($logfh),$|=1)[0]);
              };
 
 logEntry($logfh,"Cron thread started");
 # Handle INTerrupt signal
 $SIG{'INT'}=sub { 
      logEntry($logfh,"Cron INTd");
      close($logfile);
      threads->exit();
      $SIG{'INT'}='DEFAULT';
 };

 my $keepgoing=1;
 my $ping=Net::Ping->new('icmp');
 my $sql='select backup_task_id,hostname,zfs_target from backup_queue';
 my $dbh;
 my $sth;

 while ($keepgoing) {
  $dbh=DBI->connect("dbi:Pg:",,) || logEntry($logfh,'Unable to connect to database');
  $sth=$dbh->prepare($sql);
  $sth->execute() || logEntry($logfh,"Unable to run $sql: ".$dbh->errstr);
  # Get any items we need to backup
  while (my $row=$sth->fetchrow_hashref) {
   my $h=$row->{'hostname'};
   if ($ping->ping($h)) {
    my $zpool=$row->{'zfs_target'};
    $zpool =~ s/\/.*//;
    # Assign to a queue
    my $q=$qs->{$zpool};
    if (defined($q)) {
     my $logid=&stampjoinqueue($dbh,$row->{'backup_task_id'},$logfh);
     print "Enqueueing ".&shorthost($h)." task id ".$row->{'backup_task_id'}." with log id ".$logid."\n";
     logEntry($logfh,"Enqueueing ".&shorthost($h)." task id ".$row->{'backup_task_id'}." with log id ".$logid."\n");
     # Queue the item by adding the unique log ID
     $q->enqueue($logid);
    } else {
     logEntry($logfh,"Unable to enqueue entry for $h: Could not determine a VG");
    }
   } else {
    # Machine is offline
    logEntry($logfh,"Machine $h is not pingable.");
   }
  }
  $dbh->disconnect;
  # Should we quit?
  if (examinecontrolq(D_CRON) & M_STOP) {
   $keepgoing=0;
  } else {
   sleep(SLEEP_CRON);
  }
 }
 logEntry($logfh,"Cron thread ending");
}

# Utility function to get the short name from an FQDN
sub shorthost($) {
 my $hn=shift;
 $hn =~ s/\..*//;
 return $hn;
}

# Utility function to decide whether a host is alive
sub ping($) {
 my $hn=shift;
 my $p=Net::Ping->new();
 return $p->ping($hn);
}

# A thread to receive jobs from the queue and run them
sub writer($$$) {
 my $vg=shift;
 my $q=shift;
 my $ident=shift;
 print "Writer $ident started\n";
 $SIG{'INT'}=sub { print "writer $ident INTd\n";threads->exit();$SIG{'INT'}='DEFAULT';};
 # Begin a log file
 my $logfile="/var/log/chem-zfs-backup-server/$ident-thread.log";
 my $logfh;
 open $logfh,">>$logfile";
 # Unbuffer the log file
 select((select($logfh),$|=1)[0]);
 # Handle log-file closure
 $SIG{'USR1'}=sub {
      close($logfh);
      open $logfh,">>$logfile";
      select((select($logfh),$|=1)[0]);
              };
 # Handle INTerrupt signal
 $SIG{'INT'}=sub { 
      logEntry($logfh,"writer $ident INTd");
      threads->exit();
      $SIG{'INT'}='DEFAULT';
 };

 my $dbh;
 my $keepgoing=1;
 my $actions={zfs => \&zfswrite, zfs_rsync=> \&zfsrsyncwrite};
 while ($keepgoing) {
  # Get a structure from the queue
  my $qlen=$q->pending();
  if ($qlen) {
   my @items=$q->dequeue_nb();
   $dbh=DBI->connect("dbi:Pg:",,);
   my $sql="select backup_type_name from writer_view where backup_log_id=?";
   my $sth=$dbh->prepare($sql);
   foreach my $id (@items) {
    $sth->execute($id);
    # Really ought to only get one row at a time
    while (my $row=$sth->fetchrow_hashref) {
     #backup_log_id backup_item_id backup_class hostname timeout arguments age
     my $class=$row->{'backup_type_name'};
     $actions->{$class}->($dbh,$id,$logfh);
    }
   }
   $dbh->disconnect;
  }
  # Should we quit?
  if (examinecontrolq(D_WRITER) & M_STOP) {
   $keepgoing=0;
  } else {
   sleep(SLEEP_WRITER);
  }
 }
 logEntry($logfh,"Writer for $vg terminating");
}

sub dummyexecute($$$) {
 my $dbh=shift;
 my $hostname=shift;
 my $id=shift;
 my $aref=shift;
 #use Data::Dumper;
 #print "Dummy writer : will write a backup for $hostname".Dumper($aref)."\n";
}

sub zfswrite($$$) {
 my ($dbh,$id,$logfh)=@_;
 logEntry($logfh,"ZFS writer requested for job $id");
 # Look up some details about this job
 my $sql='select * from zfs_detail_view where backup_log_id=?';
 my $args=$dbh->selectrow_hashref($sql,undef,$id);
 logEntry($logfh,"ZFS writer received job $id for ".$args->{'hostname'});
 &stampbackupstart($dbh,$id);
 logEntry($logfh,"zfs sending ".$args->{'zfs_source'}." to ".$args->{'zfs_target'});
 # TODO: Write log file somewhere sensible like /var/log/backup/$hostname-$i
 my $fh;
 logEntry($logfh,"RUNNING zfs-rsnapshot.sh $args->{'hostname'} $args->{'zfs_source'} $args->{'zfs_target'} $args->{'zfs_recurse'}");
 my $ret=execandwait('/var/log/chem-zfs-backup-server/'.$args->{'hostname'}.'-queue',
                     'zfs-rsnapshot.sh',
                     $args->{'hostname'},
                     $args->{'zfs_source'},
                     $args->{'zfs_target'},
                     $args->{'zfs_recurse'}
                    );
 &stampbackupstop($dbh,$id,$ret);
 logEntry($logfh,"ZFS writer completed job $id for ".$args->{'hostname'}." - ret code $ret");
 &zfstidyup($dbh,$id,$args->{'zfs_target'},$logfh);
}

sub zfsrsyncwrite($$$) {
 my ($dbh,$id,$logfh)=@_;
 logEntry($logfh,"ZFS rsync writer requested for job $id");
 # Look up some details about this job
 my $sql='select * from zfs_rsync_detail_view where backup_log_id=?';
 my $args=$dbh->selectrow_hashref($sql,undef,$id);
 #print "ZFS rsync writer received job $id for ".$args->{'hostname'}."\n";
 &stampbackupstart($dbh,$id);
 logEntry($logfh,"zfs rsyncing ".$args->{'directory_source'}." to ".$args->{'zfs_target'});
 # TODO: Write log file somewhere sensible
 my $hf;
 logEntry($logfh,"RUNNING zfs-rsync.sh $args->{'hostname'} $args->{'directory_source'} $args->{'zfs_target'}");
 #system('zfs-rsync.sh',$args->{'hostname'},$args->{'directory_source'},$args->{'zfs_target'});
 my $ret=execandwait('/var/log/chem-zfs-backup-server/'.$args->{'hostname'}.'-queue',
                     'zfs-rsync.sh',
                     $args->{'hostname'},
                     $args->{'directory_source'},
                     $args->{'zfs_target'}
                    );
 &stampbackupstop($dbh,$id,$ret);
 logEntry($logfh,"ZFS rsync writer completed job $id for ".$args->{'hostname'}." returning $ret");
 &zfstidyup($dbh,$id,$args->{'zfs_target'},$logfh);
}

sub zfstidyup($$$$) {
 my ($dbh,$id,$zfs,$logfh)=@_;
 my $zfshost=new ZFS::Host('localhost');
 logEntry($logfh,"Tidying up snapshots on $zfs");
 my $sql='select * from zfs_pruning_view where backup_log_id=?';
 my $arr=$dbh->selectall_hashref($sql,'flagname',undef,$id);
#print "Archive Policy:";
#use Data::Dumper;
#print Dumper($arr);
#print "\n\n";
 my %archivepolicy=%$arr;
 my $config=ZFS::Config->void();
 $config->archivepolicy($arr);
 
 $zfshost->zfsName($zfs);

 my @children=$zfshost->getChildFilesystems;
 foreach my $child (@children) {
  $zfshost->zfsName($child);
  my $list=$zfshost->getListForPrune();
  #print "Currently have \n";
  #print Dumper($list);
  my $mostrecent=$zfshost->getLastSnapTime();
  foreach my $type ($config->getArchiveTypes) {
   #print "Preparing $type pass\n";
   #print "Config is:\n";
   #print Dumper($config);
   $list=&addtag($type,$mostrecent,\$config,\$list,\$zfshost);
   #print "Finished $type pass\n";
   #print Dumper($list);
  }
  #print "List\n";
  #print Dumper($list);
  #print "ZFShost\n";
  #print Dumper($zfshost);
  #print "Calling delete untagged\n";
  logEntry($logfh,"Removing untagged snapshots:");
  #print Dumper($list);
  &deleteuntagged(\$list,\$zfshost);

  #print Dumper($list);
  #use Data::Dumper;
  #print Dumper($zfshost);
  logEntry($logfh,"Tidied");
 }
}

sub execandwait($$;@) {
 my $logfile=shift;
 my $jobout;
 my $jobin;
 open ($jobout,'>>'.$logfile);
 my $pid=open3(\*CHLD_IN,$jobout,$jobout,@_);
 close (CHLD_IN);
 waitpid($pid,0);
 my $ret=$?>>8;
 return $ret;
}

# Indicate when a host has joined a queue. The returned ID identifies this backup job
sub stampjoinqueue($$$) {
 my ($dbh,$id,$logfh)=@_;
 my $sql="insert into backup_log (backup_task_id, joined_queue) values (?,now()) returning backup_log_id";
 my $sth=$dbh->prepare($sql);
 my $rv=$sth->execute($id);
 if (! $rv) {
  logEntry($logfh,"Odd, failed to insert into backup_log");
  exit;
 }
 my $rec=$sth->fetchrow_hashref();
 return $rec->{'backup_log_id'};
 #return $a[0];
}

# Indicate when a backup started
sub stampbackupstart($$) {
 my ($dbh,$id)=@_;
 my $sql="update backup_log set started_processing=now() where backup_log_id=?";
 $dbh->do($sql,undef,$id);
}

# Indicate when a backup has ended
sub stampbackupstop($$$) {
 my ($dbh,$id,$exit)=@_;
 my $sql="update backup_log set ended_processing=now(), exit_code=? where backup_log_id=?";
 $dbh->do($sql,undef,$exit,$id);
}

sub rsnapshotextrainfo($$$$) {
 my ($dbh,$id,$type,$logfh)=@_;
 logEntry($logfh,"Updating backup_log_rsnapshot backup_log_id=$id, type=$type");
 my $sql="insert into backup_log_rsnapshot (backup_log_id,rsnapshot_subtype_id) values (?,(select id from rsnapshot_subtypes where desc=?))"; 
 $dbh->do($sql,undef,$id,$type);
}

# Remove incomplete log entries
sub tidydatabase() {
 my $dbh=DBI->connect("dbi:Pg:",,);
 #my $sql='delete from backup_log where offline !=1 or offline is null and ended_processing is null';
 my $sql='delete from backup_log where ended_processing is null and ((not offline_once) or offline_once is null)';
 $dbh->do($sql,undef);
 $sql="update backup_task set isrunning='f'";
 $dbh->do($sql,undef);
 $dbh->disconnect;
}

sub setoffline($$) {
 my ($dbh,$id)=@_;
 my $sql='update backup_log set offline=1 where backup_log_id=?';
 $dbh->do($sql,undef,$id);
}

# Examine control queue
sub examinecontrolq($) {
 my $dmask=shift;
 my $return=0;
 {
  lock($controlq);
  if ($controlq->pending()) {
   my $msg=$controlq->peek();
   if (($msg & D_MASK) == $dmask) {
    $msg=$controlq->dequeue();
   }
   $return=($msg & M_MASK);
  }
 }
 return $return;
}

sub getzpools() {
 my @zpools=`zpool list -H -o name`;
 chomp(@zpools);
 return @zpools;
}

sub addtag($$$$$$) {
 	my ($type,$mostrecent,$rconfig,$rlist,$rhost,$logfh)=@_;
	my $config=$$rconfig;
	my $list=$$rlist;
	my $host=$$rhost;
    
        #print "Investigating $type tags\n";
        #print Dumper($list);
        #print "Most recent: $mostrecent\n";
#print "Config in AddTag\n";
#print Dumper($config);
        my $maxage=$mostrecent-$config->ageOfArchiveType($type);
        my $step=$config->ageOfArchiveType($type)/$config->numberOfArchiveType($type);
        my $c=&counttags($type,$list);
        # Bootstrap code
        if ($c==0) {
                # Find oldest snapshot < maximum age
                #print "Bootstrapping $type: need candidates with stamp>$maxage\n";
                my @candidates=sort(grep {$_>=($maxage)} keys(%{$list}));
                my $bootstrap=$candidates[0];
                push(@{$list->{$bootstrap}},$type);
                $host->holdSnapshot($type,$bootstrap);
                logEntry($logfh,"Bootstrapping $type by tagging $bootstrap");
                #print Dumper($list);
        }
        # Algorithm code: Find newest tagged snapshot
        my $leftmost=&newest($type,$list);
        logEntry($logfh,"Located $leftmost tagged as $type ");
        # Find any snapshots to add
        logEntry($logfh,"Seeking others newer than ".($leftmost+$step)." $step away");
        my $nextsnap=&newerthan($leftmost+$step,$list);
        if ($nextsnap) { 
                logEntry($logfh,"Found $nextsnap - tagging");
                push(@{$list->{$nextsnap}},$type);
                $host->holdSnapshot($type,$nextsnap);
                #print Dumper($list);
                &addtag($type,$mostrecent,\$config,\$list,\$host);
        }
        # De-tag any snapshots which are older than $maxage
        my @candidates=grep {$_<$maxage} keys(%{$list});
        foreach my $k (sort(@candidates)) {
                my $c=scalar(@{$list->{$k}});
                #print "$k has $c entries\n";
                if (scalar(@{$list->{$k}})>0) {
                        $host->releaseSnapshot($type,$k) if grep(/$type/, @{$list->{$k}});
#print "removing $type from $k : ".join(', ',@{$list->{$k}})."\n";
                        my @l=grep {!/$type/} @{$list->{$k}};
                        $list->{$k}=[];
                        push(@{$list->{$k}},@l);
logEntry($logfh,"removed $type from $k : ".join(', ',@{$list->{$k}}));
                }
        }
	return $list;
}

sub deleteuntagged($$) {
	#my ($list,$host)=shift;
	my $rlist=shift;
	my $list=$$rlist;
	my $rhost=shift;
	my $host=$$rhost;
#print "In deleteuntagged : list\n";
#print Dumper($list);
#print "--HOST---\n";
#print Dumper($host);
# Remove any untagged snapshots
        foreach my $k (keys(%{$list})) {
                if (scalar(@{$list->{$k}})==0) {
                        delete $list->{$k};
                        $host->destroySnapshot($k);
                }
        }
}


sub newerthan($$) {
        my ($age,$list)=@_;
        my @list=(sort(grep {$_>=$age} keys(%{$list})));

        return $list[0];
}

sub newest($$) {
        my ($type,$list)=@_;
        my @list=reverse(sort(grep { grep /$type/,@{$list->{$_}};} keys(%{$list})));
        return $list[0];
}

sub counttags($$) {
        my ($type,$list)=@_;
        #my $count=0;
        my $count=scalar(grep { grep /$type/,@{$list->{$_}};} keys(%{$list}));
        #print "Found $count $type items\n";
        return $count;
}


sub logEntry($$) {
 my ($logfh,$logText) = @_;
 my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);
 my $dateTime = sprintf "%4d-%02d-%02d %02d:%02d:%02d", $year + 1900, $mon + 1, $mday, $hour, $min, $sec;
 if ($logfh) {
  print $logfh "$dateTime $logText\n";
 }
}
