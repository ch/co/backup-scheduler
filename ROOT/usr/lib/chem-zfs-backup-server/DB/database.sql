--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: hid; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hid;


ALTER SCHEMA hid OWNER TO postgres;

--
-- Name: hotwire; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA hotwire;


ALTER SCHEMA hotwire OWNER TO postgres;

GRANT USAGE on schema hotwire to public;

-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

-- 
-- Name: backup_operator
--

CREATE ROLE backup_operator;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = hotwire, pg_catalog;

--
-- Name: suggest_view(character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: hotwire; Owner: postgres
--

CREATE FUNCTION suggest_view(_tablename character varying, _schemaname character varying, _omenuname character varying, _oviewname character varying) RETURNS SETOF character varying
    LANGUAGE plpgsql
    AS $_$
declare
colcount bigint;
keycount bigint;
colinf record;
query text;
pkeyfield varchar;
fullname varchar;
colalias varchar;
-- Type of table/view
qtype varchar;
updrule text;
delrule text;
insrule text;
insruleb text:=') values (';
insrulec text:=') RETURNING ';
hidtable varchar;
viewname varchar:=_oviewname;
menuname varchar:=_omenuname;
testschema varchar;
istable boolean;
roid integer;
rtable varchar;
rfieldnum integer;
begin
-- Viewname is strictly optional.
if (viewname is NULL) THEN
 viewname=tablename;
END IF;
-- Menuname is strictly optional.
if (menuname is NULL) THEN
 menuname='Menu';
END IF;

-- Is this a table or a view?
select count(*) from pg_catalog.pg_tables where pg_tables.schemaname=_schemaname and pg_tables.tablename=_tablename into colcount;
istable=(colcount=1);
if (istable) THEN
 qtype='table';
else
 qtype='view';
END IF;

return next '-- [II]  Hotwire suggesting a view definition for the '||qtype||' "'||_tablename||'" ';
return next '--       in the "'||_schemaname||'" schema'::varchar;
-- Check that this table exists
select count(*) from information_schema.columns where table_name=_tablename and table_schema=_schemaname into colcount;
if (colcount<1) THEN
 return next '-- [EE]  Hotwire was unable to find a definition for that '||qtype||'. Sorry.';
 EXIT;
END IF;
return next '-- [II]  Found '||colcount||' fields in that '||qtype;

-- Check that this table has a primary key
if (istable) THEN
  SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=_tablename into pkeyfield;
  if (pkeyfield IS NULL) THEN
   return next '-- [EE]  Hotwire was unable to find a primary key for that '||qtype||'. Perhaps you need to add one?';
   return;
   EXIT;
  else
   return next '-- Primary key on this '||qtype||' is '||pkeyfield;
  END IF;
ELSE
  -- How do we find a primary key for a view?
  return next '-- [II]  Attempting to identify primary key on the view '||_tablename;
  -- Find the _RETURN rule for this view
  select distinct objid from pg_depend 
                  inner join pg_rewrite on pg_depend.objid=pg_rewrite.oid 
                       where refobjid=_tablename::regclass 
                         and classid='pg_rewrite'::regclass  
                         and rulename='_RETURN'into roid;
  select count(*) from (select distinct refobjid::regclass 
                                   from pg_depend 
                                  where objid=roid 
                                    and deptype='n' 
                                    and refobjsubid!=0) a into colcount;
  if (colcount>1) THEN
    return next '-- [EE]  Sorry, hotwire cannot (yet) suggest definitions for views coming from more';
    return next '--       than one table.';
    return;
    exit;
  else 

    select distinct refobjid::regclass from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 limit 1 into rtable;
    
    return next '-- [II]  Good, this view uses a single table ('||rtable||')';
    SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=rtable into pkeyfield;
    return next '--       Checking for presence of that table''s primary key ('||pkeyfield||')';
    return next '         in the columns of the view';
    -- Find column number
    select attnum from pg_attribute where attrelid='person'::regclass and attname='id' into rfieldnum;
    -- Is column number in view?
    select count(*) from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 and refobjsubid=rfieldnum into colcount;
    if (colcount=1) then
      return next '-- [II]  Good, the primary key ('||pkeyfield||') on that table appears in the view.';
    else
      return next '-- [EE]  Oh dear, the primary key ('||pkeyfield||') doesn''t appear in the view. ';
      return next '--       Try adding it to the view and repeating this operation...';
      return;
      exit;
    end if;
  END IF;
--   execute select distinct objid from pg_depend inner join pg_rewrite on 
-- pg_depend.objid=pg_rewrite.oid where refobjid='tview'::regclass 
-- and classid='pg_rewrite'::regclass  and rulename='_RETURN';
END IF;
fullname=menuname||'/'||viewname;

query='CREATE VIEW hotwire."'||fullname||'" AS SELECT ';
updrule='CREATE RULE "hotwire_'||fullname||'_upd" AS ON UPDATE TO hotwire."'||fullname||'" DO INSTEAD UPDATE "'||_tablename||'" SET ';
delrule='CREATE RULE "hotwire_'||fullname||'_del" AS ON DELETE TO hotwire."'||fullname||'" DO INSTEAD '||
        'DELETE FROM "'||_tablename||'" WHERE "'||pkeyfield||'" = old.id;';
insrule='CREATE RULE "hotwire_'||fullname||'_ins" AS ON INSERT TO hotwire."'||fullname||'" DO INSTEAD '||
        'INSERT INTO "'||_tablename||'" (';

-- Iterate over all fields in the table
for colinf in (select * from information_schema.columns where table_name=_tablename and table_schema=_schemaname) LOOP
 colalias=NULL;
 -- return next '-- Considering field '||colinf.column_name;
  -- Only tables have foreign keys. (Well, views sort of do but...)
  IF (istable) THEN
    -- Is this a foreign key? 
    SELECT count(*) FROM information_schema.table_constraints tc 
                      LEFT JOIN information_schema.key_column_usage kcu 
                        ON tc.constraint_catalog = kcu.constraint_catalog 
                          AND tc.constraint_schema = kcu.constraint_schema 
                          AND tc.constraint_name = kcu.constraint_name 
                      LEFT JOIN information_schema.referential_constraints rc 
                        ON tc.constraint_catalog = rc.constraint_catalog 
                          AND tc.constraint_schema = rc.constraint_schema 
                          AND tc.constraint_name = rc.constraint_name 
                      LEFT JOIN information_schema.constraint_column_usage ccu 
                        ON rc.unique_constraint_catalog = ccu.constraint_catalog 
                          AND rc.unique_constraint_schema = ccu.constraint_schema 
                          AND rc.unique_constraint_name = ccu.constraint_name
                  WHERE lower(tc.constraint_type) in ('foreign key') 
                    AND kcu.column_name=colinf.column_name
                    AND tc.table_name=_tablename into keycount;
    IF (keycount>0) THEN
      if ((length(colinf.column_name)>3) AND 
          (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)!='_id')) then
        colalias=colinf.column_name||'_id';
      end if;
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
        into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema and column_name=hidtable 
          into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF; -- Is this a foreign key?  
  ELSE
    if (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)='_id') THEN
      return next '-- [II]  The column '||colinf.column_name||' will be treated as a foreign key';
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=hidtable into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF;
  END IF;
 -- Is this the primary key?
 if (colinf.column_name=pkeyfield) THEN
  if (pkeyfield!='id') THEN
   colalias='id';
  END IF;
 ELSE
  -- We don't insert to the primary key of a table.
  insrule=insrule||' "'||colinf.column_name||'", ';
  insruleb=insruleb||' new."'||coalesce(colalias, colinf.column_name)||'", ';
 END IF;
 insrulec=insrulec||' "'||_tablename||'"."'||colinf.column_name||'", ';
 query=query||' "'||colinf.column_name||'"'||coalesce(' AS "'||colalias||'"','')||',';
 updrule=updrule||'"'||colinf.column_name||'"=new."'||coalesce(colalias,colinf.column_name)||'", ';
END LOOP;
updrule=rtrim(updrule,', ')||' WHERE "'||_tablename||'"."'||pkeyfield||'"=old.id;';
insrule=rtrim(insrule,', ')||rtrim(insruleb,', ')||rtrim(insrulec,', ')||';';
query=rtrim(query,', ')||' FROM "'||_schemaname||'"."'||_tablename||'";';
return next '-- Hotwire suggests the following query definition:';
return next query::varchar;
return next '-- Rules:';
return next updrule::varchar;
return next delrule::varchar;
return next insrule::varchar;
end
$_$;


ALTER FUNCTION hotwire.suggest_view(_tablename character varying, _schemaname character varying, _omenuname character varying, _oviewname character varying) OWNER TO postgres;

--
-- Name: FUNCTION suggest_view(_tablename character varying, _schemaname character varying, _omenuname character varying, _oviewname character varying); Type: COMMENT; Schema: hotwire; Owner: postgres
--

COMMENT ON FUNCTION suggest_view(_tablename character varying, _schemaname character varying, _omenuname character varying, _oviewname character varying) IS 'To suggest a hotwire view from a table''s definition';


SET search_path = public, pg_catalog;

--
-- Name: _primarytable(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION _primarytable(_view character varying) RETURNS character varying
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
 declare
  nextview varchar;
  basetable varchar:=null;
 begin

  -- Loop over possible views/tables.
  for nextview in select regexp_replace(regexp_replace(regexp_split_to_table(definition,E'[\\.AS\ ]+id[, ]'::text),'^.* ',''),E'\\..*','') from pg_views where viewname=_view loop
   if basetable is null then
    -- return next 'Considering '||nextview;
    perform * from pg_tables where schemaname!='cache' and tablename=nextview;
    if found then
     -- return next 'Accept '||nextview;
     basetable=nextview;
    else
     perform * from pg_views where schemaname !='cache' and viewname=nextview;
     if found then
      --return next 'Recursing for '||nextview;
      basetable=_primarytable(nextview);
     else
--      return next 'Rejecting '||nextview;
     end if;
    end if;
   end if;
    -- Might be a view    
  end loop;
  return basetable;
 end
$$;


ALTER FUNCTION public._primarytable(_view character varying) OWNER TO postgres;

--
-- Name: update_ended_processing(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_ended_processing() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
declare
 
begin
 -- NB we also update the backup_log table to set started_processing; we only want to do the following updates if we have ended_processing
 if new.ended_processing is not null then
   update backup_task set last_ended=new.ended_processing where backup_task_id=new.backup_task_id;
   update backup_task set isrunning='f' where backup_task_id=new.backup_task_id and isrunning;
 end if;
 return new;
end
$$;


ALTER FUNCTION public.update_ended_processing() OWNER TO postgres;

--
-- Name: update_started_processing(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION update_started_processing() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
declare
 
begin
 update backup_task set isrunning='t' where backup_task_id=new.backup_task_id;
  return new;
end
$$;


ALTER FUNCTION public.update_started_processing() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: backup_task; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE backup_task (
    backup_task_id integer NOT NULL,
    host_id integer NOT NULL,
    backup_type_id integer NOT NULL,
    backup_task_name text NOT NULL,
    timeout_soft integer,
    timeout_hard integer,
    not_before integer,
    not_after integer,
    zfs_target text,
    soft_green numeric,
    soft_yellow numeric,
    hard_yellow numeric,
    space_red numeric,
    space_yellow numeric,
    last_ended timestamp with time zone,
    isrunning boolean DEFAULT false
);


ALTER TABLE public.backup_task OWNER TO postgres;

SET search_path = hid, pg_catalog;

--
-- Name: backup_task_hid; Type: VIEW; Schema: hid; Owner: postgres
--

CREATE VIEW backup_task_hid AS
    SELECT backup_task.backup_task_id, backup_task.backup_task_name AS backup_task_hid FROM public.backup_task;


ALTER TABLE hid.backup_task_hid OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: backup_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE backup_type (
    backup_type_id integer NOT NULL,
    backup_type_name text NOT NULL,
    details_table character varying
);


ALTER TABLE public.backup_type OWNER TO postgres;

SET search_path = hid, pg_catalog;

--
-- Name: backup_type_hid; Type: VIEW; Schema: hid; Owner: postgres
--

CREATE VIEW backup_type_hid AS
    SELECT backup_type.backup_type_id, backup_type.backup_type_name AS backup_type_hid FROM public.backup_type;


ALTER TABLE hid.backup_type_hid OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: host; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE host (
    host_id integer NOT NULL,
    hostname text NOT NULL,
    offline boolean DEFAULT true,
    disabled boolean DEFAULT false
);

ALTER TABLE host ADD CONSTRAINT uniq_host_hostname UNIQUE(hostname);

ALTER TABLE public.host OWNER TO postgres;

SET search_path = hid, pg_catalog;

--
-- Name: host_hid; Type: VIEW; Schema: hid; Owner: postgres
--

CREATE VIEW host_hid AS
    SELECT host.host_id, host.hostname AS host_hid FROM public.host;


ALTER TABLE hid.host_hid OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: zfs_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE zfs_detail (
    zfs_detail_id integer NOT NULL,
    backup_task_id integer NOT NULL,
    zfs_source text NOT NULL,
    zfs_excludes text,
    zfs_recurse boolean
);


ALTER TABLE public.zfs_detail OWNER TO postgres;

SET search_path = hid, pg_catalog;

--
-- Name: zfs_detail_hid; Type: VIEW; Schema: hid; Owner: postgres
--

CREATE VIEW zfs_detail_hid AS
    SELECT zfs_detail.zfs_detail_id, ((host.hostname || ':'::text) || zfs_detail.zfs_source) AS zfs_detail_hid FROM ((public.zfs_detail NATURAL JOIN public.backup_task) NATURAL JOIN public.host);


ALTER TABLE hid.zfs_detail_hid OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: zfs_rsync_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE zfs_rsync_detail (
    zfs_rsync_detail_id integer NOT NULL,
    backup_task_id integer NOT NULL,
    directory_source text NOT NULL
);


ALTER TABLE public.zfs_rsync_detail OWNER TO postgres;

SET search_path = hid, pg_catalog;

--
-- Name: zfs_rsync_detail_hid; Type: VIEW; Schema: hid; Owner: postgres
--

CREATE VIEW zfs_rsync_detail_hid AS
    SELECT zfs_rsync_detail.zfs_rsync_detail_id, ((host.hostname || ':'::text) || zfs_rsync_detail.directory_source) AS zfs_rsync_detail_hid FROM ((public.zfs_rsync_detail NATURAL JOIN public.backup_task) NATURAL JOIN public.host);


ALTER TABLE hid.zfs_rsync_detail_hid OWNER TO postgres;

SET search_path = hotwire, pg_catalog;

--
-- Name: hw_pref_seq; Type: SEQUENCE; Schema: hotwire; Owner: postgres
--

CREATE SEQUENCE hw_pref_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_pref_seq OWNER TO postgres;

--
-- Name: hw_Preferences; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE "hw_Preferences" (
    id integer DEFAULT nextval('hw_pref_seq'::regclass) NOT NULL,
    hw_preference_name character varying,
    hw_preference_type_id integer,
    hw_preference_const character varying
);


ALTER TABLE hotwire."hw_Preferences" OWNER TO postgres;

--
-- Name: 90_Action/Hotwire/All Possible Preferences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "90_Action/Hotwire/All Possible Preferences" AS
    SELECT "hw_Preferences".id, "hw_Preferences".hw_preference_name AS preference_name, "hw_Preferences".hw_preference_type_id FROM "hw_Preferences";


ALTER TABLE hotwire."90_Action/Hotwire/All Possible Preferences" OWNER TO postgres;

--
-- Name: hw_user_pref_seq; Type: SEQUENCE; Schema: hotwire; Owner: postgres
--

CREATE SEQUENCE hw_user_pref_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_user_pref_seq OWNER TO postgres;

--
-- Name: hw_User Preferences; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE "hw_User Preferences" (
    id integer DEFAULT nextval('hw_user_pref_seq'::regclass) NOT NULL,
    preference_id integer,
    preference_value character varying,
    user_id bigint
);


ALTER TABLE hotwire."hw_User Preferences" OWNER TO postgres;

--
-- Name: 90_Action/Hotwire/User Preferences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "90_Action/Hotwire/User Preferences" AS
    SELECT "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id AS "Postgres_User_id" FROM "hw_User Preferences";


ALTER TABLE hotwire."90_Action/Hotwire/User Preferences" OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: backup_log; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE backup_log (
    backup_log_id integer NOT NULL,
    backup_task_id integer,
    exit_code integer,
    exit_data text,
    joined_queue timestamp without time zone,
    started_processing timestamp without time zone,
    ended_processing timestamp without time zone,
    offline_once boolean
);


ALTER TABLE public.backup_log OWNER TO postgres;

--
-- Name: exitcodes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE exitcodes (
    exit_code integer,
    exit_desc character varying
);


ALTER TABLE public.exitcodes OWNER TO postgres;

SET search_path = hotwire, pg_catalog;

--
-- Name: Backups/Errors; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Errors" AS
    SELECT backup_log.backup_log_id AS id, backup_log.backup_task_id, backup_task.zfs_target, backup_log.joined_queue, backup_log.started_processing, backup_log.ended_processing, exitcodes.exit_desc, backup_log.exit_code, date_part('epoch'::text, (backup_log.ended_processing - backup_log.started_processing)) AS runtime, date_part('epoch'::text, (backup_log.started_processing - backup_log.joined_queue)) AS latency FROM ((public.backup_log NATURAL JOIN public.backup_task) NATURAL LEFT JOIN public.exitcodes) WHERE (backup_log.exit_code <> 0) ORDER BY backup_log.joined_queue DESC, backup_log.started_processing DESC, backup_log.ended_processing DESC;


ALTER TABLE hotwire."Backups/Errors" OWNER TO postgres;

--
-- Name: Backups/Hosts; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Hosts" AS
    SELECT host.host_id AS id, host.hostname, host.disabled, host.offline FROM public.host;


ALTER TABLE hotwire."Backups/Hosts" OWNER TO postgres;

--
-- Name: Backups/Last_Exit_Codes; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Last_Exit_Codes" AS
    SELECT backup_task.backup_task_id AS id, backup_task.host_id, backup_task.backup_type_id, backup_task.backup_task_name, (SELECT backup_log.exit_code FROM public.backup_log WHERE ((backup_log.backup_task_id = backup_task.backup_task_id) AND (backup_log.ended_processing IS NOT NULL)) ORDER BY backup_log.ended_processing DESC LIMIT 1) AS exit_code FROM public.backup_task;


ALTER TABLE hotwire."Backups/Last_Exit_Codes" OWNER TO postgres;

--
-- Name: Backups/Logs; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Logs" AS
    SELECT backup_log.backup_log_id AS id, backup_log.backup_task_id, backup_task.zfs_target, backup_log.joined_queue, backup_log.started_processing, backup_log.ended_processing, exitcodes.exit_desc, backup_log.exit_code, date_part('epoch'::text, (backup_log.ended_processing - backup_log.started_processing)) AS runtime, date_part('epoch'::text, (backup_log.started_processing - backup_log.joined_queue)) AS latency FROM ((public.backup_log NATURAL JOIN public.backup_task) NATURAL LEFT JOIN public.exitcodes) ORDER BY backup_log.joined_queue DESC, backup_log.started_processing DESC, backup_log.ended_processing DESC;


ALTER TABLE hotwire."Backups/Logs" OWNER TO postgres;

--
-- Name: Backups/Queue; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Queue" AS
    SELECT backup_log.backup_log_id AS id, substr(backup_task.zfs_target, 0, strpos(backup_task.zfs_target, '/'::text)) AS zpool, backup_log.backup_task_id, backup_log.exit_code, backup_log.exit_data, backup_log.joined_queue, backup_log.started_processing, backup_log.ended_processing, backup_log.offline_once FROM (public.backup_log NATURAL JOIN public.backup_task) WHERE (backup_log.ended_processing IS NULL) ORDER BY backup_log.joined_queue DESC;


ALTER TABLE hotwire."Backups/Queue" OWNER TO postgres;

--
-- Name: Backups/Tasks/All; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Tasks/All" AS
    SELECT backup_task.backup_task_id AS id, backup_task.host_id, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.zfs_target, backup_task.not_after, backup_task.not_before, backup_task.space_yellow, backup_task.space_red, backup_task.hard_yellow, backup_task.soft_yellow, backup_task.soft_green, backup_task.timeout_hard, backup_task.timeout_soft FROM public.backup_task;


ALTER TABLE hotwire."Backups/Tasks/All" OWNER TO postgres;

--
-- Name: Backups/Tasks/ZFS; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Tasks/ZFS" AS
    SELECT backup_task.backup_task_id AS id, backup_task.host_id, backup_task.backup_type_id AS ro_backup_type_id, backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target AS "ZFS_target", backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow, zfs_detail.zfs_detail_id AS _zfs_detail_id, zfs_detail.zfs_source AS "ZFS_source", zfs_detail.zfs_excludes AS "ZFS_excludes", zfs_detail.zfs_recurse AS "ZFS_recurse" FROM (public.backup_task NATURAL JOIN public.zfs_detail) WHERE (backup_task.backup_type_id = 1);


ALTER TABLE hotwire."Backups/Tasks/ZFS" OWNER TO postgres;

--
-- Name: Backups/Tasks/ZFS_Rsync; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Backups/Tasks/ZFS_Rsync" AS
    SELECT backup_task.backup_task_name, backup_task.backup_task_id AS id, backup_task.host_id, zfs_rsync_detail.directory_source, backup_task.zfs_target AS "ZFS_target", backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow FROM (public.backup_task LEFT JOIN public.zfs_rsync_detail USING (backup_task_id)) WHERE (backup_task.backup_type_id = 3);


ALTER TABLE hotwire."Backups/Tasks/ZFS_Rsync" OWNER TO postgres;

--
-- Name: Postgres_User_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW "Postgres_User_hid" AS
    SELECT (pg_roles.oid)::bigint AS "Postgres_User_id", pg_roles.rolname AS "Postgres_User_hid" FROM pg_roles;


ALTER TABLE hotwire."Postgres_User_hid" OWNER TO postgres;

--
-- Name: _column_data; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _column_data AS
    SELECT pg_attribute.attname, pg_type.typname, pg_attribute.atttypmod, pg_class.relname, pg_attribute.attnotnull, pg_attribute.atthasdef, (pg_type.typelem > (0)::oid) AS "array", (SELECT child.typname FROM pg_type child WHERE (child.oid = pg_type.typelem)) AS elementtype FROM (((pg_attribute JOIN pg_type ON ((pg_attribute.atttypid = pg_type.oid))) JOIN pg_class ON ((pg_attribute.attrelid = pg_class.oid))) JOIN pg_namespace ON ((pg_class.relnamespace = pg_namespace.oid))) WHERE (((pg_namespace.nspname = 'public'::name) OR (pg_namespace.nspname = 'hotwire'::name)) AND (pg_attribute.attnum > 0));


ALTER TABLE hotwire._column_data OWNER TO postgres;

--
-- Name: hw_preference_type_seq; Type: SEQUENCE; Schema: hotwire; Owner: postgres
--

CREATE SEQUENCE hw_preference_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hotwire.hw_preference_type_seq OWNER TO postgres;

--
-- Name: hw_preference_type_hid; Type: TABLE; Schema: hotwire; Owner: postgres; Tablespace: 
--

CREATE TABLE hw_preference_type_hid (
    hw_preference_type_id integer DEFAULT nextval('hw_preference_type_seq'::regclass) NOT NULL,
    hw_preference_type_hid character varying
);


ALTER TABLE hotwire.hw_preference_type_hid OWNER TO postgres;

--
-- Name: _preferences; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _preferences AS
    SELECT "Preferences".hw_preference_const AS preference_name, a.preference_value, hw_preference_type_hid.hw_preference_type_hid AS preference_type FROM ((((SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM "hw_User Preferences" "User Preferences" WHERE ("User Preferences".user_id IS NULL) UNION ALL SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM (((pg_auth_members m JOIN pg_roles b ON ((m.roleid = b.oid))) JOIN pg_roles r ON ((m.member = r.oid))) JOIN "hw_User Preferences" "User Preferences" ON ((b.oid = ("User Preferences".user_id)::oid))) WHERE (r.rolname = "current_user"())) UNION ALL SELECT "User Preferences".preference_id, "User Preferences".preference_value FROM (pg_roles JOIN "hw_User Preferences" "User Preferences" ON ((pg_roles.oid = ("User Preferences".user_id)::oid))) WHERE (pg_roles.rolname = "current_user"())) a JOIN "hw_Preferences" "Preferences" ON ((a.preference_id = "Preferences".id))) JOIN hw_preference_type_hid USING (hw_preference_type_id));


ALTER TABLE hotwire._preferences OWNER TO postgres;

--
-- Name: _role_data; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _role_data AS
    SELECT member.rolname AS member, role.rolname AS role FROM ((pg_roles role LEFT JOIN pg_auth_members ON ((role.oid = pg_auth_members.roleid))) RIGHT JOIN pg_roles member ON ((member.oid = pg_auth_members.member)));


ALTER TABLE hotwire._role_data OWNER TO postgres;


--
-- Name: _view_data; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW _view_data AS
    SELECT pgns.nspname AS schema, pg_class.relname AS view, pg_class.relacl AS perms, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'insert'::text) AS insert, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'update'::text) AS update, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'delete'::text) AS delete, has_table_privilege((((('"'::text || (pgns.nspname)::text) || '"."'::text) || (pg_class.relname)::text) || '"'::text), 'select'::text) AS "select", ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '4'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS delete_rule, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS insert_rule, (SELECT ((r.ev_action)::text ~~ '%returningList:%'::text) FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '3'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) AS insert_returning, ((SELECT count(*) AS count FROM ((pg_rewrite r JOIN pg_class c ON ((c.oid = r.ev_class))) LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace))) WHERE (((r.ev_type = '2'::"char") AND (r.rulename <> '_RETURN'::name)) AND (c.relname = pg_class.relname))) > 0) AS update_rule, pg_views.definition, public._primarytable((pg_class.relname)::character varying) AS primary_table, CASE WHEN (regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ ('%ORDER BY%'::character varying)::text) THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text) ELSE NULL::text END AS order_by FROM ((pg_class JOIN pg_namespace pgns ON ((pg_class.relnamespace = pgns.oid))) JOIN pg_views ON ((pg_class.relname = pg_views.viewname))) WHERE ((pg_class.relname ~~ '%/%'::text) AND (pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name])));


ALTER TABLE hotwire._view_data OWNER TO postgres;

--
-- Name: preference_hid; Type: VIEW; Schema: hotwire; Owner: postgres
--

CREATE VIEW preference_hid AS
    SELECT "Preferences".id AS preference_id, "Preferences".hw_preference_name AS preference_hid FROM "hw_Preferences" "Preferences";


ALTER TABLE hotwire.preference_hid OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- Name: a; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW a AS
    SELECT backup_task.host_id, backup_task.backup_task_id, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target, backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow, host.hostname, host.offline, host.disabled, date_part('epoch'::text, (now() - ((SELECT max(backup_log.joined_queue) AS max FROM backup_log WHERE (((backup_log.backup_task_id = backup_task.backup_task_id) AND (backup_log.ended_processing IS NOT NULL)) AND (backup_log.exit_code = 0))))::timestamp with time zone)) AS age FROM (backup_task NATURAL JOIN host) WHERE (host.disabled = false);


ALTER TABLE public.a OWNER TO postgres;

--
-- Name: backup_job; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW backup_job AS
    SELECT backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target, host.hostname, backup_type.backup_type_name FROM ((backup_task NATURAL JOIN host) NATURAL JOIN backup_type);


ALTER TABLE public.backup_job OWNER TO postgres;

--
-- Name: backup_log_backup_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE backup_log_backup_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backup_log_backup_log_id_seq OWNER TO postgres;

--
-- Name: backup_log_backup_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE backup_log_backup_log_id_seq OWNED BY backup_log.backup_log_id;


--
-- Name: backup_queue; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW backup_queue AS
    SELECT a.host_id, a.backup_task_id, a.backup_type_id, a.backup_task_name, a.timeout_soft, a.timeout_hard, a.not_before, a.not_after, a.zfs_target, a.soft_green, a.soft_yellow, a.hard_yellow, a.space_red, a.space_yellow, a.hostname, a.offline, a.disabled, a.age FROM a WHERE (((((((((a.not_before IS NULL) OR (a.not_after IS NULL)) AND (a.age > (a.timeout_soft)::double precision)) OR ((((a.not_before < a.not_after) AND (date_part('hour'::text, now()) >= (a.not_before)::double precision)) AND (date_part('hour'::text, now()) < (a.not_after)::double precision)) AND (a.age > (a.timeout_soft)::double precision))) OR (((a.not_before > a.not_after) AND ((date_part('hour'::text, now()) >= (a.not_before)::double precision) OR (date_part('hour'::text, now()) < (a.not_after)::double precision))) AND (a.age > (a.timeout_soft)::double precision))) OR (a.age > (a.timeout_hard)::double precision)) OR (a.age IS NULL)) AND (a.offline = false)) AND ((SELECT count(*) AS count FROM backup_log WHERE ((backup_log.backup_task_id = a.backup_task_id) AND (backup_log.ended_processing IS NULL))) < 1)) ORDER BY a.age DESC;


ALTER TABLE public.backup_queue OWNER TO postgres;

--
-- Name: backup_queue_new; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW backup_queue_new AS
    SELECT a.host_id, a.backup_task_id, a.backup_type_id, a.backup_task_name, a.timeout_soft, a.timeout_hard, a.not_before, a.not_after, a.zfs_target, a.soft_green, a.soft_yellow, a.hard_yellow, a.space_red, a.space_yellow, a.hostname, a.offline, a.disabled, a.age FROM (SELECT backup_task.host_id, backup_task.isrunning, backup_task.backup_task_id, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target, backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow, backup_task.last_ended, host.hostname, host.offline, host.disabled, date_part('seconds'::text, (now() - backup_task.last_ended)) AS age FROM (backup_task NATURAL JOIN host) WHERE ((NOT host.offline) AND (NOT host.disabled))) a WHERE ((NOT a.isrunning) AND ((((((((a.not_before IS NULL) OR (a.not_after IS NULL)) AND (a.age > (a.timeout_soft)::double precision)) OR ((((a.not_before < a.not_after) AND (date_part('hour'::text, now()) >= (a.not_before)::double precision)) AND (date_part('hour'::text, now()) < (a.not_after)::double precision)) AND (a.age > (a.timeout_soft)::double precision))) OR ((a.not_before > a.not_after) AND (date_part('hour'::text, now()) >= (a.not_before)::double precision))) OR ((date_part('hour'::text, now()) < (a.not_after)::double precision) AND (a.age > (a.timeout_soft)::double precision))) OR (a.age > (a.timeout_hard)::double precision)) OR (a.age IS NULL))) ORDER BY a.age DESC;


ALTER TABLE public.backup_queue_new OWNER TO postgres;

--
-- Name: backup_task_backup_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE backup_task_backup_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backup_task_backup_task_id_seq OWNER TO postgres;

--
-- Name: backup_task_backup_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE backup_task_backup_task_id_seq OWNED BY backup_task.backup_task_id;


--
-- Name: backup_type_backup_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE backup_type_backup_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.backup_type_backup_type_id_seq OWNER TO postgres;

--
-- Name: backup_type_backup_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE backup_type_backup_type_id_seq OWNED BY backup_type.backup_type_id;


--
-- Name: hobbit_hosts; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hobbit_hosts AS
    SELECT host.hostname FROM host WHERE (NOT host.disabled);


ALTER TABLE public.hobbit_hosts OWNER TO postgres;

--
-- Name: most_recent_backup_log; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW most_recent_backup_log AS
    SELECT backup_log.backup_task_id, max(backup_log.backup_log_id) AS backup_log_id FROM backup_log WHERE ((backup_log.exit_code = 0) AND (backup_log.ended_processing IS NOT NULL)) GROUP BY backup_log.backup_task_id;


ALTER TABLE public.most_recent_backup_log OWNER TO postgres;

--
-- Name: zfs_pruning; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE zfs_pruning (
    zfs_pruning_id integer NOT NULL,
    backup_task_id integer NOT NULL,
    zfs_pruning_age integer NOT NULL,
    zfs_pruning_count integer NOT NULL,
    flagname text
);


ALTER TABLE public.zfs_pruning OWNER TO postgres;

--
-- Name: hobbit_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hobbit_view AS
    SELECT most_recent_backup_log.backup_task_id, host.hostname, host.offline, host.disabled, backup_task.backup_task_name, (COALESCE(backup_task.soft_green, (2)::numeric) * (backup_task.timeout_soft)::numeric) AS green_soft, (COALESCE(backup_task.soft_yellow, (4)::numeric) * (backup_task.timeout_soft)::numeric) AS yellow_soft, (COALESCE(backup_task.hard_yellow, (6)::numeric) * (backup_task.timeout_soft)::numeric) AS yellow_hard, date_part('epoch'::text, (now() - (backup_log.ended_processing)::timestamp with time zone)) AS age, date_part('epoch'::text, (backup_log.joined_queue - backup_log.started_processing)) AS wait, date_part('epoch'::text, (backup_log.ended_processing - backup_log.started_processing)) AS exec, backup_task.zfs_target, COALESCE(backup_task.space_red, 0.95) AS space_red, COALESCE(backup_task.space_yellow, 0.90) AS space_yellow, (SELECT count(*) AS count FROM zfs_pruning z WHERE (z.backup_task_id = z.backup_task_id)) AS prune_count FROM (((most_recent_backup_log NATURAL JOIN backup_task) NATURAL JOIN backup_log) NATURAL JOIN host);


ALTER TABLE public.hobbit_view OWNER TO postgres;

--
-- Name: hobbit_view_old; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hobbit_view_old AS
    SELECT b.backup_task_id, host.hostname, host.offline, host.disabled, a.backup_task_name, (COALESCE(a.soft_green, (2)::numeric) * (a.timeout_soft)::numeric) AS green_soft, (COALESCE(a.soft_yellow, (4)::numeric) * (a.timeout_soft)::numeric) AS yellow_soft, (COALESCE(a.hard_yellow, (6)::numeric) * (a.timeout_hard)::numeric) AS yellow_hard, date_part('epoch'::text, (now() - (b.ended_processing)::timestamp with time zone)) AS age, date_part('epoch'::text, (b.joined_queue - b.started_processing)) AS wait, date_part('epoch'::text, (b.ended_processing - b.started_processing)) AS exec, a.zfs_target, COALESCE(a.space_red, 0.95) AS space_red, COALESCE(a.space_yellow, 0.90) AS space_yellow, (SELECT count(*) AS count FROM zfs_pruning z WHERE (z.backup_task_id = a.backup_task_id)) AS prune_count FROM ((backup_log b NATURAL JOIN backup_task a) NATURAL JOIN host) WHERE (b.backup_log_id = (SELECT max(c.backup_log_id) AS max FROM backup_log c WHERE (((c.exit_code = 0) AND (b.backup_task_id = c.backup_task_id)) AND (c.ended_processing IS NOT NULL))));


ALTER TABLE public.hobbit_view_old OWNER TO postgres;

--
-- Name: host_host_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE host_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.host_host_id_seq OWNER TO postgres;

--
-- Name: host_host_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE host_host_id_seq OWNED BY host.host_id;


--
-- Name: raw_all_backup_items; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW raw_all_backup_items AS
    SELECT backup_task.backup_task_id, backup_type.backup_type_name, host.hostname, backup_task.timeout_soft, backup_task.timeout_hard, date_part('epoch'::text, (now() - ((SELECT max(backup_log.joined_queue) AS max FROM backup_log WHERE (backup_log.backup_task_id = backup_task.backup_task_id)))::timestamp with time zone)) AS age FROM ((backup_task NATURAL JOIN backup_type) NATURAL JOIN host);


ALTER TABLE public.raw_all_backup_items OWNER TO postgres;

--
-- Name: runtime_detail; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW runtime_detail AS
    SELECT backup_log.backup_task_id, backup_task.backup_task_name, "substring"(backup_task.zfs_target, 1, 6) AS zpool, avg((backup_log.ended_processing - backup_log.started_processing)) AS runtime FROM ((backup_log NATURAL JOIN backup_task) NATURAL JOIN host) WHERE (NOT host.disabled) GROUP BY backup_log.backup_task_id, backup_task.backup_task_name, backup_task.zfs_target;


ALTER TABLE public.runtime_detail OWNER TO postgres;

--
-- Name: testviewacl; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW testviewacl AS
    SELECT host.host_id, host.hostname, host.offline, host.disabled FROM host;


ALTER TABLE public.testviewacl OWNER TO postgres;

--
-- Name: time_summary; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW time_summary AS
    SELECT a.zpool, sum(a.t) AS runtime FROM (SELECT count(*) AS count, "substring"(backup_task.zfs_target, 1, 6) AS zpool, backup_log.backup_task_id, avg((backup_log.ended_processing - backup_log.started_processing)) AS t FROM ((backup_log NATURAL JOIN backup_task) NATURAL JOIN host) WHERE (NOT host.disabled) GROUP BY backup_log.backup_task_id, backup_task.zfs_target) a GROUP BY a.zpool;


ALTER TABLE public.time_summary OWNER TO postgres;

--
-- Name: writer_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW writer_view AS
    SELECT backup_log.backup_log_id, backup_type.backup_type_name FROM ((backup_log NATURAL JOIN backup_task) NATURAL JOIN backup_type);


ALTER TABLE public.writer_view OWNER TO postgres;

--
-- Name: zfs_detail_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW zfs_detail_view AS
    SELECT backup_task.host_id, backup_log.offline_once AS offline, backup_task.backup_task_id, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target, backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow, zfs_detail.zfs_detail_id, zfs_detail.zfs_source, zfs_detail.zfs_recurse, zfs_detail.zfs_excludes, backup_log.backup_log_id, backup_log.exit_code, backup_log.exit_data, backup_log.joined_queue, backup_log.started_processing, backup_log.ended_processing, host.hostname, host.disabled FROM (((backup_task NATURAL JOIN zfs_detail) NATURAL JOIN backup_log) NATURAL JOIN host);


ALTER TABLE public.zfs_detail_view OWNER TO postgres;

--
-- Name: zfs_detail_zfs_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE zfs_detail_zfs_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zfs_detail_zfs_detail_id_seq OWNER TO postgres;

--
-- Name: zfs_detail_zfs_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE zfs_detail_zfs_detail_id_seq OWNED BY zfs_detail.zfs_detail_id;


--
-- Name: zfs_pruning_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW zfs_pruning_view AS
    SELECT backup_log.backup_log_id, zfs_pruning.zfs_pruning_age AS age, zfs_pruning.zfs_pruning_count AS count, zfs_pruning.flagname FROM (backup_log NATURAL JOIN zfs_pruning);


ALTER TABLE public.zfs_pruning_view OWNER TO postgres;

--
-- Name: zfs_pruning_zfs_pruning_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE zfs_pruning_zfs_pruning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zfs_pruning_zfs_pruning_id_seq OWNER TO postgres;

--
-- Name: zfs_pruning_zfs_pruning_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE zfs_pruning_zfs_pruning_id_seq OWNED BY zfs_pruning.zfs_pruning_id;


--
-- Name: zfs_rsync_detail_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW zfs_rsync_detail_view AS
    SELECT backup_task.host_id, backup_log.offline_once, zfs_rsync_detail.backup_task_id, zfs_rsync_detail.zfs_rsync_detail_id, zfs_rsync_detail.directory_source, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.timeout_soft, backup_task.timeout_hard, backup_task.not_before, backup_task.not_after, backup_task.zfs_target, backup_task.soft_green, backup_task.soft_yellow, backup_task.hard_yellow, backup_task.space_red, backup_task.space_yellow, backup_log.backup_log_id, backup_log.exit_code, backup_log.exit_data, backup_log.joined_queue, backup_log.started_processing, backup_log.ended_processing, host.hostname, host.disabled FROM (((zfs_rsync_detail NATURAL JOIN backup_task) NATURAL JOIN backup_log) NATURAL JOIN host);


ALTER TABLE public.zfs_rsync_detail_view OWNER TO postgres;

--
-- Name: zfs_rsync_detail_zfs_rsync_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zfs_rsync_detail_zfs_rsync_detail_id_seq OWNER TO postgres;

--
-- Name: zfs_rsync_detail_zfs_rsync_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq OWNED BY zfs_rsync_detail.zfs_rsync_detail_id;


--
-- Name: backup_log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_log ALTER COLUMN backup_log_id SET DEFAULT nextval('backup_log_backup_log_id_seq'::regclass);


--
-- Name: backup_task_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_task ALTER COLUMN backup_task_id SET DEFAULT nextval('backup_task_backup_task_id_seq'::regclass);


--
-- Name: backup_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_type ALTER COLUMN backup_type_id SET DEFAULT nextval('backup_type_backup_type_id_seq'::regclass);


--
-- Name: host_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY host ALTER COLUMN host_id SET DEFAULT nextval('host_host_id_seq'::regclass);


--
-- Name: zfs_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_detail ALTER COLUMN zfs_detail_id SET DEFAULT nextval('zfs_detail_zfs_detail_id_seq'::regclass);


--
-- Name: zfs_pruning_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_pruning ALTER COLUMN zfs_pruning_id SET DEFAULT nextval('zfs_pruning_zfs_pruning_id_seq'::regclass);


--
-- Name: zfs_rsync_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_rsync_detail ALTER COLUMN zfs_rsync_detail_id SET DEFAULT nextval('zfs_rsync_detail_zfs_rsync_detail_id_seq'::regclass);


SET search_path = hotwire, pg_catalog;

--
-- Name: Preferences_pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "hw_Preferences"
    ADD CONSTRAINT "Preferences_pkey" PRIMARY KEY (id);


--
-- Name: hw_User Preferences.pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "hw_User Preferences"
    ADD CONSTRAINT "hw_User Preferences.pkey" PRIMARY KEY (id);


--
-- Name: hw_preference_type_pkey; Type: CONSTRAINT; Schema: hotwire; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);


SET search_path = public, pg_catalog;

--
-- Name: backup_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY backup_log
    ADD CONSTRAINT backup_log_pkey PRIMARY KEY (backup_log_id);


--
-- Name: backup_task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY backup_task
    ADD CONSTRAINT backup_task_pkey PRIMARY KEY (backup_task_id);


--
-- Name: backup_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY backup_type
    ADD CONSTRAINT backup_type_pkey PRIMARY KEY (backup_type_id);


--
-- Name: host_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY host
    ADD CONSTRAINT host_pkey PRIMARY KEY (host_id);


--
-- Name: zfs_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY zfs_detail
    ADD CONSTRAINT zfs_detail_pkey PRIMARY KEY (zfs_detail_id);


--
-- Name: zfs_pruning_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY zfs_pruning
    ADD CONSTRAINT zfs_pruning_pkey PRIMARY KEY (zfs_pruning_id);


--
-- Name: zfs_rsync_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY zfs_rsync_detail
    ADD CONSTRAINT zfs_rsync_detail_pkey PRIMARY KEY (zfs_rsync_detail_id);


--
-- Name: backup_log_ended_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX backup_log_ended_idx ON backup_log USING btree (ended_processing);


--
-- Name: backup_log_task_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX backup_log_task_idx ON backup_log USING btree (backup_task_id);


--
-- Name: hostname_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX hostname_idx ON host USING btree (hostname);


SET search_path = hotwire, pg_catalog;

--
-- Name: backup_tasks_zfs_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE backup_tasks_zfs_del AS ON DELETE TO "Backups/Tasks/ZFS" DO INSTEAD (DELETE FROM public.zfs_detail WHERE (zfs_detail.backup_task_id = old.id); DELETE FROM public.backup_task WHERE (backup_task.backup_task_id = old.id); );


--
-- Name: backups_tasks_zfs_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE backups_tasks_zfs_ins AS ON INSERT TO "Backups/Tasks/ZFS" DO INSTEAD (INSERT INTO public.backup_task (host_id, backup_type_id, backup_task_name, timeout_soft, timeout_hard, not_before, not_after, zfs_target, soft_green, soft_yellow, hard_yellow, space_red, space_yellow) VALUES (new.host_id, 1, new.backup_task_name, new.timeout_soft, new.timeout_hard, new.not_before, new.not_after, new."ZFS_target", new.soft_green, new.soft_yellow, new.hard_yellow, new.space_red, new.space_yellow); INSERT INTO public.zfs_detail (backup_task_id, zfs_source, zfs_excludes, zfs_recurse) VALUES (currval('public.backup_task_backup_task_id_seq'::regclass), new."ZFS_source", new."ZFS_excludes", new."ZFS_recurse"); );


--
-- Name: backups_tasks_zfs_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE backups_tasks_zfs_upd AS ON UPDATE TO "Backups/Tasks/ZFS" DO INSTEAD (UPDATE public.backup_task SET host_id = new.host_id, backup_task_name = new.backup_task_name, timeout_soft = new.timeout_soft, timeout_hard = new.timeout_hard, not_before = new.not_before, not_after = new.not_after, zfs_target = new."ZFS_target", soft_green = new.soft_green, soft_yellow = new.soft_yellow, hard_yellow = new.hard_yellow, space_red = new.space_red, space_yellow = new.space_yellow WHERE (backup_task.backup_task_id = old.id); UPDATE public.zfs_detail SET zfs_source = new."ZFS_source", zfs_excludes = new."ZFS_excludes", zfs_recurse = new."ZFS_recurse" WHERE (zfs_detail.backup_task_id = old.id); );


--
-- Name: hotwire_Backups/Hosts_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Hosts_del" AS ON DELETE TO "Backups/Hosts" DO INSTEAD DELETE FROM public.host WHERE (host.host_id = old.id);


--
-- Name: hotwire_Backups/Hosts_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Hosts_ins" AS ON INSERT TO "Backups/Hosts" DO INSTEAD INSERT INTO public.host (disabled, offline, hostname) VALUES (new.disabled, new.offline, new.hostname) RETURNING host.host_id, host.hostname, host.disabled, host.offline;


--
-- Name: hotwire_Backups/Hosts_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Hosts_upd" AS ON UPDATE TO "Backups/Hosts" DO INSTEAD UPDATE public.host SET disabled = new.disabled, offline = new.offline, hostname = new.hostname WHERE (host.host_id = old.id);


--
-- Name: hotwire_Backups/Tasks_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Tasks_del" AS ON DELETE TO "Backups/Tasks/All" DO INSTEAD DELETE FROM public.backup_task WHERE (backup_task.backup_task_id = old.id);


--
-- Name: hotwire_Backups/Tasks_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Tasks_ins" AS ON INSERT TO "Backups/Tasks/All" DO INSTEAD INSERT INTO public.backup_task (space_yellow, space_red, hard_yellow, soft_yellow, soft_green, zfs_target, not_after, not_before, timeout_hard, timeout_soft, backup_task_name, backup_type_id, host_id) VALUES (new.space_yellow, new.space_red, new.hard_yellow, new.soft_yellow, new.soft_green, new.zfs_target, new.not_after, new.not_before, new.timeout_hard, new.timeout_soft, new.backup_task_name, new.backup_type_id, new.host_id) RETURNING backup_task.backup_task_id, backup_task.host_id, backup_task.backup_type_id, backup_task.backup_task_name, backup_task.zfs_target, backup_task.not_after, backup_task.not_before, backup_task.space_yellow, backup_task.space_red, backup_task.hard_yellow, backup_task.soft_yellow, backup_task.soft_green, backup_task.timeout_hard, backup_task.timeout_soft;


--
-- Name: hotwire_Backups/Tasks_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE "hotwire_Backups/Tasks_upd" AS ON UPDATE TO "Backups/Tasks/All" DO INSTEAD UPDATE public.backup_task SET space_yellow = new.space_yellow, space_red = new.space_red, hard_yellow = new.hard_yellow, soft_yellow = new.soft_yellow, soft_green = new.soft_green, zfs_target = new.zfs_target, not_after = new.not_after, not_before = new.not_before, timeout_hard = new.timeout_hard, timeout_soft = new.timeout_soft, backup_task_name = new.backup_task_name, backup_type_id = new.backup_type_id, host_id = new.host_id WHERE (backup_task.backup_task_id = old.id);


--
-- Name: hotwire_backups_tasks_zfs_rsync_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_backups_tasks_zfs_rsync_del AS ON DELETE TO "Backups/Tasks/ZFS_Rsync" DO INSTEAD (DELETE FROM public.zfs_rsync_detail WHERE (zfs_rsync_detail.backup_task_id = old.id); DELETE FROM public.backup_task WHERE (backup_task.backup_task_id = old.id); );


--
-- Name: hotwire_backups_tasks_zfs_rsync_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_backups_tasks_zfs_rsync_ins AS ON INSERT TO "Backups/Tasks/ZFS_Rsync" DO INSTEAD (INSERT INTO public.backup_task (host_id, backup_type_id, backup_task_name, timeout_soft, timeout_hard, not_before, not_after, zfs_target, soft_green, soft_yellow, hard_yellow, space_red, space_yellow) VALUES (new.host_id, 1, new.backup_task_name, new.timeout_soft, new.timeout_hard, new.not_before, new.not_after, new."ZFS_target", new.soft_green, new.soft_yellow, new.hard_yellow, new.space_red, new.space_yellow); INSERT INTO public.zfs_rsync_detail (backup_task_id, directory_source) VALUES (currval('public.backup_task_backup_task_id_seq'::regclass), new.directory_source); );


--
-- Name: hotwire_backups_tasks_zfs_rsync_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hotwire_backups_tasks_zfs_rsync_upd AS ON UPDATE TO "Backups/Tasks/ZFS_Rsync" DO INSTEAD (UPDATE public.backup_task SET host_id = new.host_id, backup_task_name = new.backup_task_name, timeout_soft = new.timeout_soft, timeout_hard = new.timeout_hard, not_before = new.not_before, not_after = new.not_after, zfs_target = new."ZFS_target", soft_green = new.soft_green, soft_yellow = new.soft_yellow, hard_yellow = new.hard_yellow, space_red = new.space_red, space_yellow = new.space_yellow WHERE (backup_task.backup_task_id = old.id); UPDATE public.zfs_rsync_detail SET directory_source = new.directory_source WHERE (zfs_rsync_detail.backup_task_id = old.id); );


--
-- Name: hw_super_admin_preferences_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_super_admin_preferences_del AS ON DELETE TO "90_Action/Hotwire/All Possible Preferences" DO INSTEAD DELETE FROM "hw_Preferences" WHERE ("hw_Preferences".id = old.id);


--
-- Name: hw_super_admin_preferences_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_super_admin_preferences_ins AS ON INSERT TO "90_Action/Hotwire/All Possible Preferences" DO INSTEAD INSERT INTO "hw_Preferences" (hw_preference_name, hw_preference_type_id) VALUES (new.preference_name, new.hw_preference_type_id);


--
-- Name: hw_super_admin_preferences_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_super_admin_preferences_upd AS ON UPDATE TO "90_Action/Hotwire/All Possible Preferences" DO INSTEAD UPDATE "hw_Preferences" SET hw_preference_name = new.preference_name, hw_preference_type_id = new.hw_preference_type_id WHERE ("hw_Preferences".id = old.id);


--
-- Name: hw_user_preferences_del; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_del AS ON DELETE TO "90_Action/Hotwire/User Preferences" DO INSTEAD DELETE FROM "hw_User Preferences" WHERE ("hw_User Preferences".id = old.id);


--
-- Name: hw_user_preferences_ins; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_ins AS ON INSERT TO "90_Action/Hotwire/User Preferences" DO INSTEAD INSERT INTO "hw_User Preferences" (preference_id, preference_value, user_id) VALUES (new.preference_id, new.preference_value, new."Postgres_User_id");


--
-- Name: hw_user_preferences_upd; Type: RULE; Schema: hotwire; Owner: postgres
--

CREATE RULE hw_user_preferences_upd AS ON UPDATE TO "90_Action/Hotwire/User Preferences" DO INSTEAD UPDATE "hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new."Postgres_User_id" WHERE ("hw_User Preferences".id = old.id);


SET search_path = public, pg_catalog;

--
-- Name: backup_log_insert; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER backup_log_insert AFTER INSERT ON backup_log FOR EACH ROW EXECUTE PROCEDURE update_started_processing();


--
-- Name: backup_log_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER backup_log_update AFTER UPDATE ON backup_log FOR EACH ROW EXECUTE PROCEDURE update_ended_processing();


SET search_path = hotwire, pg_catalog;

--
-- Name: hw_pref_type_fkey; Type: FK CONSTRAINT; Schema: hotwire; Owner: postgres
--

ALTER TABLE ONLY "hw_Preferences"
    ADD CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id) REFERENCES hw_preference_type_hid(hw_preference_type_id);


SET search_path = public, pg_catalog;

--
-- Name: backup_log_backup_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_log
    ADD CONSTRAINT backup_log_backup_task_id_fkey FOREIGN KEY (backup_task_id) REFERENCES backup_task(backup_task_id) ON DELETE CASCADE;


--
-- Name: backup_task_backup_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_task
    ADD CONSTRAINT backup_task_backup_type_id_fkey FOREIGN KEY (backup_type_id) REFERENCES backup_type(backup_type_id);


--
-- Name: backup_task_host_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY backup_task
    ADD CONSTRAINT backup_task_host_id_fkey FOREIGN KEY (host_id) REFERENCES host(host_id) ON DELETE CASCADE;


--
-- Name: zfs_detail_backup_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_detail
    ADD CONSTRAINT zfs_detail_backup_task_id_fkey FOREIGN KEY (backup_task_id) REFERENCES backup_task(backup_task_id) ON DELETE CASCADE;


--
-- Name: zfs_pruning_backup_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_pruning
    ADD CONSTRAINT zfs_pruning_backup_task_id_fkey FOREIGN KEY (backup_task_id) REFERENCES backup_task(backup_task_id) ON DELETE CASCADE;


--
-- Name: zfs_rsync_detail_backup_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zfs_rsync_detail
    ADD CONSTRAINT zfs_rsync_detail_backup_task_id_fkey FOREIGN KEY (backup_task_id) REFERENCES backup_task(backup_task_id) ON DELETE CASCADE;


--
-- Name: hid; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA hid FROM PUBLIC;
REVOKE ALL ON SCHEMA hid FROM postgres;
GRANT ALL ON SCHEMA hid TO postgres;


--
-- Name: hotwire; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA hotwire FROM PUBLIC;
REVOKE ALL ON SCHEMA hotwire FROM postgres;
GRANT ALL ON SCHEMA hotwire TO postgres;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT ALL ON SCHEMA public TO backup_operator;


--
-- Name: _primarytable(character varying); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION _primarytable(_view character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION _primarytable(_view character varying) FROM postgres;
GRANT ALL ON FUNCTION _primarytable(_view character varying) TO postgres;
GRANT ALL ON FUNCTION _primarytable(_view character varying) TO PUBLIC;


--
-- Name: backup_task; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_task FROM PUBLIC;
REVOKE ALL ON TABLE backup_task FROM postgres;
GRANT ALL ON TABLE backup_task TO postgres;
GRANT ALL ON TABLE backup_task TO backup_operator;


--
-- Name: backup_type; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_type FROM PUBLIC;
REVOKE ALL ON TABLE backup_type FROM postgres;
GRANT ALL ON TABLE backup_type TO postgres;
GRANT ALL ON TABLE backup_type to backup_operator;

--
-- Name: host; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE host FROM PUBLIC;
REVOKE ALL ON TABLE host FROM postgres;
GRANT ALL ON TABLE host TO postgres;
GRANT ALL ON TABLE host TO backup_operator;


--
-- Name: zfs_detail; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_detail FROM PUBLIC;
REVOKE ALL ON TABLE zfs_detail FROM postgres;
GRANT ALL ON TABLE zfs_detail TO postgres;
GRANT ALL ON TABLE zfs_detail TO backup_operator;


--
-- Name: zfs_rsync_detail; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_rsync_detail FROM PUBLIC;
REVOKE ALL ON TABLE zfs_rsync_detail FROM postgres;
GRANT ALL ON TABLE zfs_rsync_detail TO postgres;
GRANT ALL ON TABLE zfs_rsync_detail TO backup_operator;


SET search_path = hotwire, pg_catalog;

--
-- Name: hw_pref_seq; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON SEQUENCE hw_pref_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE hw_pref_seq FROM postgres;
GRANT ALL ON SEQUENCE hw_pref_seq TO postgres;


--
-- Name: hw_Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "hw_Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "hw_Preferences" FROM postgres;
GRANT ALL ON TABLE "hw_Preferences" TO postgres;


--
-- Name: 90_Action/Hotwire/All Possible Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "90_Action/Hotwire/All Possible Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "90_Action/Hotwire/All Possible Preferences" FROM postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/All Possible Preferences" TO postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/All Possible Preferences" TO backup_operator;


--
-- Name: hw_user_pref_seq; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON SEQUENCE hw_user_pref_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE hw_user_pref_seq FROM postgres;
GRANT ALL ON SEQUENCE hw_user_pref_seq TO postgres;


--
-- Name: hw_User Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "hw_User Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "hw_User Preferences" FROM postgres;
GRANT ALL ON TABLE "hw_User Preferences" TO postgres;


--
-- Name: 90_Action/Hotwire/User Preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "90_Action/Hotwire/User Preferences" FROM PUBLIC;
REVOKE ALL ON TABLE "90_Action/Hotwire/User Preferences" FROM postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/User Preferences" TO postgres;
GRANT ALL ON TABLE "90_Action/Hotwire/User Preferences" TO backup_operator;


SET search_path = public, pg_catalog;

--
-- Name: backup_log; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_log FROM PUBLIC;
REVOKE ALL ON TABLE backup_log FROM postgres;
GRANT ALL ON TABLE backup_log TO postgres;
GRANT ALL ON TABLE backup_log TO backup_operator;
GRANT ALL ON TABLE backup_log TO backup_operator;


--
-- Name: exitcodes; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE exitcodes FROM PUBLIC;
REVOKE ALL ON TABLE exitcodes FROM postgres;
GRANT ALL ON TABLE exitcodes TO postgres;
GRANT ALL ON TABLE exitcodes TO backup_operator;


SET search_path = hotwire, pg_catalog;

--
-- Name: Backups/Errors; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Errors" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Errors" FROM postgres;
GRANT ALL ON TABLE "Backups/Errors" TO postgres;
GRANT ALL ON TABLE "Backups/Errors" TO backup_operator;


--
-- Name: Backups/Hosts; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Hosts" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Hosts" FROM postgres;
GRANT ALL ON TABLE "Backups/Hosts" TO postgres;
GRANT ALL ON TABLE "Backups/Hosts" TO backup_operator;


--
-- Name: Backups/Last_Exit_Codes; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Last_Exit_Codes" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Last_Exit_Codes" FROM postgres;
GRANT ALL ON TABLE "Backups/Last_Exit_Codes" TO postgres;
GRANT ALL ON TABLE "Backups/Last_Exit_Codes" TO backup_operator;


--
-- Name: Backups/Logs; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Logs" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Logs" FROM postgres;
GRANT ALL ON TABLE "Backups/Logs" TO postgres;
GRANT ALL ON TABLE "Backups/Logs" TO backup_operator;


--
-- Name: Backups/Queue; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Queue" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Queue" FROM postgres;
GRANT ALL ON TABLE "Backups/Queue" TO postgres;
GRANT ALL ON TABLE "Backups/Queue" TO backup_operator;


--
-- Name: Backups/Tasks/All; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Tasks/All" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Tasks/All" FROM postgres;
GRANT ALL ON TABLE "Backups/Tasks/All" TO postgres;
GRANT ALL ON TABLE "Backups/Tasks/All" TO backup_operator;


--
-- Name: Backups/Tasks/ZFS; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Tasks/ZFS" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Tasks/ZFS" FROM postgres;
GRANT ALL ON TABLE "Backups/Tasks/ZFS" TO postgres;
GRANT ALL ON TABLE "Backups/Tasks/ZFS" TO backup_operator;


--
-- Name: Backups/Tasks/ZFS_Rsync; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Backups/Tasks/ZFS_Rsync" FROM PUBLIC;
REVOKE ALL ON TABLE "Backups/Tasks/ZFS_Rsync" FROM postgres;
GRANT ALL ON TABLE "Backups/Tasks/ZFS_Rsync" TO postgres;
GRANT ALL ON TABLE "Backups/Tasks/ZFS_Rsync" TO backup_operator;


--
-- Name: Postgres_User_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE "Postgres_User_hid" FROM PUBLIC;
REVOKE ALL ON TABLE "Postgres_User_hid" FROM postgres;
GRANT ALL ON TABLE "Postgres_User_hid" TO postgres;
GRANT ALL ON TABLE "Postgres_User_hid" TO backup_operator;


--
-- Name: _column_data; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _column_data FROM PUBLIC;
REVOKE ALL ON TABLE _column_data FROM postgres;
GRANT ALL ON TABLE _column_data TO postgres;
GRANT ALL ON TABLE _column_data TO backup_operator;


--
-- Name: hw_preference_type_seq; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON SEQUENCE hw_preference_type_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE hw_preference_type_seq FROM postgres;
GRANT ALL ON SEQUENCE hw_preference_type_seq TO postgres;


--
-- Name: hw_preference_type_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE hw_preference_type_hid FROM PUBLIC;
REVOKE ALL ON TABLE hw_preference_type_hid FROM postgres;
GRANT ALL ON TABLE hw_preference_type_hid TO postgres;


--
-- Name: _preferences; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _preferences FROM PUBLIC;
REVOKE ALL ON TABLE _preferences FROM postgres;
GRANT ALL ON TABLE _preferences TO postgres;
GRANT SELECT ON TABLE _preferences TO PUBLIC;
GRANT ALL ON TABLE _preferences TO backup_operator;


--
-- Name: _role_data; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _role_data FROM PUBLIC;
REVOKE ALL ON TABLE _role_data FROM postgres;
GRANT ALL ON TABLE _role_data TO postgres;
GRANT SELECT ON TABLE _role_data TO PUBLIC;
GRANT ALL ON TABLE _role_data TO backup_operator;


--
-- Name: _view_data; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE _view_data FROM PUBLIC;
REVOKE ALL ON TABLE _view_data FROM postgres;
GRANT ALL ON TABLE _view_data TO postgres;
GRANT ALL ON TABLE _view_data TO backup_operator;


--
-- Name: preference_hid; Type: ACL; Schema: hotwire; Owner: postgres
--

REVOKE ALL ON TABLE preference_hid FROM PUBLIC;
REVOKE ALL ON TABLE preference_hid FROM postgres;
GRANT ALL ON TABLE preference_hid TO postgres;
GRANT ALL ON TABLE preference_hid TO backup_operator;


SET search_path = public, pg_catalog;

--
-- Name: a; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE a FROM PUBLIC;
REVOKE ALL ON TABLE a FROM postgres;
GRANT ALL ON TABLE a TO postgres;
GRANT ALL ON TABLE a TO backup_operator;


--
-- Name: backup_job; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_job FROM PUBLIC;
REVOKE ALL ON TABLE backup_job FROM postgres;
GRANT ALL ON TABLE backup_job TO postgres;
GRANT ALL ON TABLE backup_job TO backup_operator;
GRANT ALL ON TABLE backup_job TO backup_operator;


--
-- Name: backup_log_backup_log_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE backup_log_backup_log_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE backup_log_backup_log_id_seq FROM postgres;
GRANT ALL ON SEQUENCE backup_log_backup_log_id_seq TO postgres;
GRANT ALL ON SEQUENCE backup_log_backup_log_id_seq TO backup_operator;
GRANT ALL ON SEQUENCE backup_log_backup_log_id_seq TO backup_operator;


--
-- Name: backup_queue; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_queue FROM PUBLIC;
REVOKE ALL ON TABLE backup_queue FROM postgres;
GRANT ALL ON TABLE backup_queue TO postgres;
GRANT ALL ON TABLE backup_queue TO backup_operator;
GRANT ALL ON TABLE backup_queue TO backup_operator;


--
-- Name: backup_queue_new; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE backup_queue_new FROM PUBLIC;
REVOKE ALL ON TABLE backup_queue_new FROM postgres;
GRANT ALL ON TABLE backup_queue_new TO postgres;
GRANT ALL ON TABLE backup_queue_new TO backup_operator;


--
-- Name: backup_task_backup_task_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE backup_task_backup_task_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE backup_task_backup_task_id_seq FROM postgres;
GRANT ALL ON SEQUENCE backup_task_backup_task_id_seq TO postgres;
GRANT ALL ON SEQUENCE backup_task_backup_task_id_seq TO backup_operator;


--
-- Name: backup_type_backup_type_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE backup_type_backup_type_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE backup_type_backup_type_id_seq FROM postgres;
GRANT ALL ON SEQUENCE backup_type_backup_type_id_seq TO postgres;
GRANT ALL ON SEQUENCE backup_type_backup_type_id_seq TO backup_operator;


--
-- Name: hobbit_hosts; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE hobbit_hosts FROM PUBLIC;
REVOKE ALL ON TABLE hobbit_hosts FROM postgres;
GRANT ALL ON TABLE hobbit_hosts TO postgres;
GRANT SELECT ON TABLE hobbit_hosts TO PUBLIC;
GRANT ALL ON TABLE hobbit_hosts TO backup_operator;


--
-- Name: zfs_pruning; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_pruning FROM PUBLIC;
REVOKE ALL ON TABLE zfs_pruning FROM postgres;
GRANT ALL ON TABLE zfs_pruning TO postgres;
GRANT ALL ON TABLE zfs_pruning TO backup_operator;


--
-- Name: hobbit_view; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE hobbit_view FROM PUBLIC;
REVOKE ALL ON TABLE hobbit_view FROM postgres;
GRANT ALL ON TABLE hobbit_view TO postgres;
GRANT SELECT ON TABLE hobbit_view TO PUBLIC;
GRANT ALL ON TABLE hobbit_view TO backup_operator;


--
-- Name: host_host_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE host_host_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE host_host_id_seq FROM postgres;
GRANT ALL ON SEQUENCE host_host_id_seq TO postgres;
GRANT ALL ON SEQUENCE host_host_id_seq TO backup_operator;


--
-- Name: raw_all_backup_items; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE raw_all_backup_items FROM PUBLIC;
REVOKE ALL ON TABLE raw_all_backup_items FROM postgres;
GRANT ALL ON TABLE raw_all_backup_items TO postgres;
GRANT ALL ON TABLE raw_all_backup_items TO backup_operator;


--
-- Name: testviewacl; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE testviewacl FROM PUBLIC;
REVOKE ALL ON TABLE testviewacl FROM postgres;
GRANT ALL ON TABLE testviewacl TO postgres;
GRANT ALL ON TABLE testviewacl TO backup_operator;


--
-- Name: writer_view; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE writer_view FROM PUBLIC;
REVOKE ALL ON TABLE writer_view FROM postgres;
GRANT ALL ON TABLE writer_view TO postgres;
GRANT ALL ON TABLE writer_view TO backup_operator;
GRANT ALL ON TABLE writer_view TO backup_operator;


--
-- Name: zfs_detail_view; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_detail_view FROM PUBLIC;
REVOKE ALL ON TABLE zfs_detail_view FROM postgres;
GRANT ALL ON TABLE zfs_detail_view TO postgres;
GRANT ALL ON TABLE zfs_detail_view TO backup_operator;
GRANT ALL ON TABLE zfs_detail_view TO backup_operator;


--
-- Name: zfs_detail_zfs_detail_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE zfs_detail_zfs_detail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE zfs_detail_zfs_detail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE zfs_detail_zfs_detail_id_seq TO postgres;
GRANT ALL ON SEQUENCE zfs_detail_zfs_detail_id_seq TO backup_operator;


--
-- Name: zfs_pruning_view; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_pruning_view FROM PUBLIC;
REVOKE ALL ON TABLE zfs_pruning_view FROM postgres;
GRANT ALL ON TABLE zfs_pruning_view TO postgres;
GRANT ALL ON TABLE zfs_pruning_view TO backup_operator;
GRANT ALL ON TABLE zfs_pruning_view TO backup_operator;


--
-- Name: zfs_pruning_zfs_pruning_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE zfs_pruning_zfs_pruning_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE zfs_pruning_zfs_pruning_id_seq FROM postgres;
GRANT ALL ON SEQUENCE zfs_pruning_zfs_pruning_id_seq TO postgres;
GRANT ALL ON SEQUENCE zfs_pruning_zfs_pruning_id_seq TO backup_operator;


--
-- Name: zfs_rsync_detail_view; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE zfs_rsync_detail_view FROM PUBLIC;
REVOKE ALL ON TABLE zfs_rsync_detail_view FROM postgres;
GRANT ALL ON TABLE zfs_rsync_detail_view TO postgres;
GRANT ALL ON TABLE zfs_rsync_detail_view TO backup_operator;


--
-- Name: zfs_rsync_detail_zfs_rsync_detail_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq TO postgres;
GRANT ALL ON SEQUENCE zfs_rsync_detail_zfs_rsync_detail_id_seq TO backup_operator;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Data for Name: backup_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO backup_type VALUES (1, 'zfs', 'zfs_detail');
INSERT INTO backup_type VALUES (3, 'zfs_rsync', 'zfs_rsync_detail');

