#!/bin/bash
# To build the excludes file using a well-known script on the target
HN=$1
SRC=$2
DST=$3
TFILE=$(mktemp)
trap "rm -f ${TFILE}" exit
set -e
set -xv
if [ -n "$SSHCONFIGFILE" ] ; then
  SSH="ssh -F $SSHCONFIGFILE -o ConnectTimeout=10"
  SCP="scp -F $SSHCONFIGFILE"
else
  SSH="ssh"
  SCP="scp"
fi
$SSH $HN /usr/local/lib/chem-rsync-backup-client/prepare >${TFILE}
if ! cmp ${TFILE} /etc/chem-zfs-backup-server/zfs-rsync.d/$HN/exclude ; then
  mkdir -p /etc/chem-zfs-backup-server/zfs-rsync.d/$HN/
  cp ${TFILE} /etc/chem-zfs-backup-server/zfs-rsync.d/$HN/exclude
fi
