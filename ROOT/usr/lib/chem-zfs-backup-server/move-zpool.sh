#!/bin/bash
# To move the contents of a zpool, perhaps because the underlying disk is failing
# This is incomplete and untested
echo "This script is incomplete and should not be used yet"
exit 1

set -e
set -x

# Load some settings
. /etc/default/zfs-backup

function dosql() {
 SQL="$1"
 psql -q -t -c "$SQL"
}

# 
if [ -z "$2" ] ; then
 echo Usage: $0 zpool new-zpool
 echo Moves the content of old-zpool to new-zpool
 exit 1
fi

OZ=$1
NZ=$2

# need to try to unexport nfs here, but failure isn't a worry
set +e
EXPORTS=`dosql "select zfs_target from backup_task where backup_task_id in (select backup_task_id from backup_task where zfs_target ~'^$OZ')"`
for EXPORT in $EXPORTS ; do
  exportfs -u $FQDN:/$EXPORT
done
set -e

echo moving $FQDN from $SOURCE to $TGT
RUNNING=`dosql "select count(*) from backup_log natural join backup_task where backup_task.zfs_target ~ '^$OZ' and ended_processing is null and started_processing is not null"`
RUNNING=`echo $RUNNING` # chomp
#echo RUNNING $RUNNING
if ! [ "$RUNNING" = 0 ] ;  then
 echo $OZ has a job running
 exit 3
fi

# De-queue jobs for this zpool
dosql "delete from backup_log where backup_task_id in (select backup_task_id from backup_task where backup_task.zfs_target ~ '^$OZ') and ended_processing is null and started_processing is null;"

# Update tasks
dosql "update backup_task set zfs_target=regexp_replace(zfs_target,'^$OZ','$NZ') where backup_task.zfs_target ~ '^$OZ')"


# Move ZFSes. Have to do this one at a time as some intermediates may already exist
TIME=`date +%s`
zfs snapshot -r $OZ@$TIME
zfs hold zfs-moving $OZ@$TIME

for ZC in $(zfs list -H -r -oname $OZ|grep -v reserved); do
# check if this ZFS exists on NZ
# if it doesn't, move it. Need to figure out the form of the target - sub NZ for OZ and strip off last component?
  SIZE=`zfs list -H -o used $ZC`
  echo Sending ZFS $ZC [ $SIZE]
  zfs send -$ $ZC@${TIME} | mbuffer | zfs receive -e $NZ
done

# Tidy up
#zpool destroy $OZ
